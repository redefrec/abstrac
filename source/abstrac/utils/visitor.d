module abstrac.utils.visitor;

import abstrac.ast : Nodes, AAstNode;

interface IVisitor {
    static foreach (Node; Nodes.All) {
        void visit(Node);
    }
}

abstract class TVisitor(bool restricted) : IVisitor {
    protected void visitChildren(AAstNode arg) {
        auto children = arg.children.dup();
        foreach (child; children) {
            if (child.parent is arg) {
                child.accept(this);
            }
        }
    }

    static foreach (Node; Nodes.All) {
        void visit(Node node) {
            static if (restricted) {
                throw new Exception(node.toString() ~ " is not allowed during " ~ typeid(this).name);
            } else {
                visitChildren(node);
            }
        }
    }
}
