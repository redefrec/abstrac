module abstrac.utils.multi_dispatch;

import std.traits;
import std.meta;

private alias ReturnTypes(Callables...) = staticMap!(ReturnType, Callables);
private alias CommonReturnType(Callables...) = CommonType!(ReturnTypes!(Callables));

class DispatchException : Exception {
    this(string message) {
        super(message);
    }
}

private struct MultiDispatcher(bool throwOnFail, Callables...) {
    static assert(Callables.length > 0);

    Callables callables;

    Select!(throwOnFail, CommonReturnType!Callables, bool) opCall(Args...)(Args args) {
        ulong[Args.length] argTypeHashes;

        static foreach (i, arg; args) {
            argTypeHashes[i] = typeid(arg).toHash();
        }

        static foreach (j, Callable; Callables) {
            {
                alias Params = Parameters!Callable;
                static assert(Params.length == Args.length);
                ulong[Params.length] paramTypeHashes;
                static foreach (i, Param; Params) {
                    paramTypeHashes[i] = typeid(Param).toHash();
                }
                if (paramTypeHashes == argTypeHashes) {
                    Params typeCastArguments;
                    static foreach (i, Param; Params) {
                        typeCastArguments[i] = cast(Param) args[i];
                    }
                    static if (is(typeof(return) == void)) {
                        callables[j](typeCastArguments);
                        return;
                    } else {
                        static if (throwOnFail) {
                            return callables[j](typeCastArguments);
                        } else {
                            callables[j](typeCastArguments);
                            return true;
                        }
                    }
                }
            }
        }
        static if (throwOnFail) {
            string argsString = "";
            static foreach (arg; args) {
                argsString ~= arg.toString() ~ ", ";
            }
            throw new DispatchException("Unmatched dispatch: " ~ argsString ~ "!");
        } else {
            return false;
        }
    }
}

MultiDispatcher!(true, Callables) dispatch(Callables...)(Callables callables) {
    MultiDispatcher!(true, Callables) dispatcher;
    dispatcher.callables = callables;
    return dispatcher;
}

MultiDispatcher!(false, Callables) tryDispatch(Callables...)(Callables callables) {
    MultiDispatcher!(false, Callables) dispatcher;
    dispatcher.callables = callables;
    return dispatcher;
}
