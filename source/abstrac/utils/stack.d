module abstrac.utils.stack;

struct Stack(T) {
    import std.algorithm : remove;
    import std.typecons : tuple;

    T[] array;

    void push(T element) {
        array ~= element;
    }

    @property size_t length() {
        return array.length;
    }

    ref T back() {
        return array[$ - 1];
    }

    T pop() {
        assert(length > 0);
        T top = back();
        array = array.remove(length - 1);
        return top;
    }

    T[] pop(size_t count) {
        assert(length >= count);
        T[] result = array[(length - count) .. length];
        array = array.remove(tuple((length - count), length));
        return result;
    }

    T popLast() {
        assert(length == 1);
        return pop();
    }
}
