module abstrac.processed_values;

public import abstrac.processed_types;
import llvm;
import std.bigint : BigInt;
import std.conv : to;
import std.algorithm : map;
import std.range : join;
import std.meta : AliasSeq;

abstract class ProcessedValue : ProcessedElement {
    abstract ProcessedType getType();
}

class ProcessedIntegerValue : ProcessedValue {
    ProcessedIntegerType type;
    BigInt value;

    mixin ProcessedValueMixin!("type", "value");
}

class ProcessedStringValue : ProcessedValue {
    string value;

    override ProcessedType getType() {
        return ProcessedPointerType.get(ProcessedIntegerType.get(false, 8));
    }

    mixin ProcessedValueMixin!("value");
}

class ProcessedRealValue : ProcessedValue {
    ProcessedRealType type;
    real value;

    mixin ProcessedValueMixin!("type", "value");
}

class ProcessedFunctionValue : ProcessedValue {
    import abstrac.ast : FunctionDeclaration;

    ProcessedFunctionType type;
    FunctionDeclaration declaration;

    mixin ProcessedValueMixin!("type", "declaration");
}

private mixin template ProcessedValueMixin(memberNames...) {

    private this() {
    }

    static typeof(this) get(Args...)(Args args) {
        static assert(Args.length == memberNames.length);
        typeof(this) result = new typeof(this);
        static foreach (i, memberName; memberNames) {
            {
                auto memptr = &__traits(getMember, result, memberName); // @suppress(dscanner.suspicious.unused_variable)
                static assert(is(Args[i] : typeof(*memptr)));
                *memptr = args[i];
            }
        }
        return result;
    }

    override bool opEquals(Object other) const {
        auto asThisType = cast(typeof(this)) other;
        if (asThisType is null) {
            return false;
        }
        static foreach (memberName; memberNames) {
            if (__traits(getMember, this, memberName) != __traits(getMember, asThisType, memberName)) {
                return false;
            }
        }
        return true;
    }

    override string toString() const {
        import std.range : repeat;
        import std.conv : to;

        string result = __traits(identifier, typeof(this)) ~ " { ";
        size_t i = 0;
        static foreach (memberName; memberNames) {
            if (i++ > 0) {
                result ~= ", ";
            }
            result ~= memberName ~ ": " ~ to!string(__traits(getMember, this, memberName));
        }
        result ~= " }";
        return result;
    }

    override size_t toHash() {
        import std.traits : fullyQualifiedName;

        size_t result = hashOf(fullyQualifiedName!(typeof(this)));
        static if (is(typeof(type))) {
            result *= type.toHash() + 78_433;
        }
        static foreach (memberName; memberNames) {
            result *= hashOf(__traits(getMember, this, memberName)) + 78_433;
        }
        return result;
    }

    static if (is(typeof(type))) {
        override @property ProcessedType getType() {
            return type;
        }
    }
}
