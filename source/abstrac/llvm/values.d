module abstrac.llvm.values;

import abstrac.processed_types : ProcessedType;
import llvm;

abstract class ABuiltValue {
abstract:
    @property ProcessedType type();
    @property LLVMValueRef value();
}

final class BuiltValue : ABuiltValue {
    import abstrac.processed_types : ProcessedType;

    ProcessedType _type;
    LLVMValueRef _value;

    this(ProcessedType type, LLVMValueRef value) {
        this._type = type;
        this._value = value;
    }

override:
    @property ProcessedType type() {
        return _type;
    }

    @property LLVMValueRef value() {
        return _value;
    }
}

class ValueScope {
    ValueScope parent;
    private ABuiltValue[string] values;

    this(ValueScope parent) {
        this.parent = parent;
    }

    void addParameter(string name, ProcessedType type, LLVMValueRef value) {
        assert(name !in values);
        values[name] = new BuiltValue(type, value);
    }

    void addSSA(string name, ProcessedType type, LLVMValueRef value) {
        assert(name !in values);
        values[name] = new BuiltValue(type, value);
    }

    ABuiltValue getValue(string name) {
        if (auto inValues = name in values) {
            return *inValues;
        } else if (parent !is null) {
            return parent.getValue(name);
        } else {
            return null;
        }
    }
}
