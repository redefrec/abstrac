module abstrac.llvm.modules;

import abstrac.llvm.functions;
import abstrac.llvm.types;
import abstrac.llvm.value_builder;
import abstrac.type_processor;
import abstrac.ast.helpers;
import abstrac.ast;
import abstrac.processed_types;
import llvm;
import std.string : toStringz;
import core.thread : Fiber;

LLVMModuleRef buildModule(Unit unit) {
    ModuleBuilder builder = new ModuleBuilder(unit);
    builder.buildAll();
    return builder.module_;
}

struct FunctionState {
    Fiber fiber;
    FunctionDeclaration declaration;
    LLVMValueRef value;
    ValueScope scope_;
}

interface IModuleCache {
}

class ModuleBuilder {
    import std.container.array : Array;

    Unit unit;
    LLVMModuleRef module_;
    ValueScope scope_;
    private IModuleCache[string] caches;

    FunctionBuilder functionBuilder;
    ValueBuilder valueBuilder;

    FunctionState[FunctionDeclaration] functions;

    this(Unit unit) {
        this.unit = unit;
        this.scope_ = new ValueScope(null);
        this.module_ = LLVMModuleCreateWithName(toStringz(unit.value));
        functionBuilder = new FunctionBuilder(this);
        valueBuilder = new ValueBuilder(this);
    }

    private void buildAll() {
        FunctionDeclaration[] funDecls = pathSelect!FunctionDeclaration(unit);
        foreach (funDecl; funDecls) {
            requireFunction(funDecl);
        }
        LLVMVerifyModule(module_, LLVMAbortProcessAction, null);
        LLVMWriteBitcodeToFile(module_, toStringz("./" ~ unit.value ~ ".bc"));
    }

    T getOrCreateCache(T)() {
        import std.traits : fullyQualifiedName;

        if (auto existing = fullyQualifiedName!T in caches) {
            return cast(T)*existing;
        } else {
            return cast(T)(caches[fullyQualifiedName!T] = new T);
        }
    }

    ABuiltValue buildValue(AAstNode arg, LLVMBuilderRef builder,
            ValueScope scope_, ProcessedType type) {
        return valueBuilder.build(arg, builder, scope_, type);
    }

    private FunctionState* pushFunctionState(FunctionDeclaration arg) {
        FunctionState* state = &(functions[arg] = FunctionState());
        state.declaration = arg;
        state.scope_ = new ValueScope(scope_);
        // state.fiber = new Fiber({ functionBuilder.build(state); });
        return state;
    }

    private FunctionState* getFunctionState(FunctionDeclaration arg) {
        return arg in functions;
    }

    LLVMValueRef requireFunction(FunctionDeclaration arg) {
        import abstrac.codegen.evaluation : Evaluator;

        FunctionState* state = getFunctionState(arg);
        if (state is null) {
            state = pushFunctionState(arg);
            // state.fiber.call();
            functionBuilder.build(state);
        }
        Evaluator(this).registerFunction(arg, state.value);
        return state.value;
    }

}

static this() {
    LLVMInitializeAllTargets();
    LLVMInitializeAllTargetInfos();
    LLVMInitializeAllTargetMCs();
    LLVMInitializeAllAsmParsers();
    LLVMInitializeAllAsmPrinters();
    LLVMInitializeAllDisassemblers();
}
