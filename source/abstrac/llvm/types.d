module abstrac.llvm.types;

import abstrac.llvm.modules : ModuleBuilder, IModuleCache;
import abstrac.processed_types;
import abstrac.type_processor;
import abstrac.ast;
import llvm;
import std.algorithm, std.array;

struct TypeBuilder {
    ModuleBuilder mBuilder;

    this(ModuleBuilder mBuilder) {
        this.mBuilder = mBuilder;
    }

    LLVMTypeRef build(FunctionDeclaration arg) {
        auto processedType = TypeProcessor(mBuilder).processDeclaration(arg);
        auto returnType = build(processedType.returnType, false);
        auto paramTypes = processedType.paramTypes.map!(a => build(a, false)).array;
        return LLVMFunctionType(returnType, paramTypes.ptr,
                cast(uint) paramTypes.length, processedType.variadic ? 1 : 0);
    }

    LLVMTypeRef build(Type arg, bool defined) {
        auto processedType = TypeProcessor(mBuilder).process(arg);
        return build(processedType, defined);
    }

    LLVMTypeRef build(ProcessedType arg, bool defined) {
        import abstrac.utils.multi_dispatch : dispatch;

        LLVMTypeRef result;

        void handleStruct(ProcessedStructType arg) {
            import std.string, std.algorithm, std.array;

            Cache cache = mBuilder.getOrCreateCache!Cache();
            if (auto cached = arg.declaration in cache.llvmStructs) {
                result = *cached;
                return;
            }
            auto context = LLVMGetModuleContext(mBuilder.module_);
            auto created = LLVMStructCreateNamed(context, toStringz(arg.declaration.value));
            cache.llvmStructs[arg.declaration] = created;
            if (arg.memberTypes !is null) {
                auto types = map!(a => build(a, true))(arg.memberTypes).array;
                LLVMStructSetBody(created, types.ptr, cast(uint) types.length, 0);
            }
            result = created;
        }

        void handleTuple(ProcessedTupleType arg) {
            import std.string, std.algorithm, std.array;

            auto types = map!(a => build(a, true))(arg.memberTypes).array;
            result = LLVMStructType(types.ptr, cast(uint) types.length, 0);
        }

        void handleFunction(ProcessedFunctionType arg) {
            import std.string, std.algorithm, std.array;

            auto returnType = build(arg.returnType, true);
            auto paramTypes = arg.paramTypes.map!(a => build(a, defined)).array;
            result = LLVMFunctionType(returnType, paramTypes.ptr,
                    cast(uint) paramTypes.length, arg.variadic ? 1 : 0);
            result = LLVMPointerType(result, 0);
        }

        dispatch((ProcessedVoidType arg) { result = LLVMVoidType(); }, (ProcessedIntegerType arg) {
            result = LLVMIntType(arg.bitWidth);
        }, (ProcessedRealType arg) {
            final switch (arg.bitWidth) {
            case 16:
                result = LLVMHalfType();
                break;
            case 32:
                result = LLVMFloatType();
                break;
            case 64:
                result = LLVMDoubleType();
                break;
            }
        }, (ProcessedPointerType arg) {
            result = LLVMPointerType(build(arg.pointedType, defined), 0);
        }, (ProcessedArrayType arg) {
            result = LLVMArrayType(build(arg.elementType, defined), cast(uint) arg.size);
        }, &handleFunction, &handleTuple, &handleStruct)(arg);
        return result;
    }
}

private class Cache : IModuleCache {
    LLVMTypeRef[StructDeclaration] llvmStructs;
}
