module abstrac.llvm.value_builder;

public import abstrac.llvm.values;
import abstrac.llvm.modules : ModuleBuilder, IModuleCache;
import abstrac.llvm.types : TypeBuilder;
import abstrac.codegen.overloads;
import abstrac.processed_values;
import abstrac.utils.visitor : TVisitor;
import abstrac.utils.multi_dispatch;
import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.type_processor;
import llvm;
import std.string : toStringz;
import std.typecons : WhiteHole;

class ValueBuilder {
    ModuleBuilder mBuilder;

    this(ModuleBuilder mBuilder) {
        this.mBuilder = mBuilder;
    }

    ABuiltValue build(AAstNode arg, LLVMBuilderRef builder, ValueScope scope_, ProcessedType type) {
        Visitor visitor = new Visitor(builder, scope_);
        return visitor.getBuiltValue(arg, type);
    }

    private class Visitor : TVisitor!true {
        import abstrac.utils.stack : Stack;

        Stack!ABuiltValue stack;
        Stack!ProcessedType typeStack;
        LLVMBuilderRef builder;
        ValueScope scope_;
        TypeProcessor typeProcessor;

        this(LLVMBuilderRef builder, ValueScope scope_) {
            this.builder = builder;
            this.scope_ = scope_;
            this.typeProcessor = TypeProcessor(mBuilder);
        }

        override void visit(BoundIdentifier arg) {
            if (auto inScope = scope_.getValue(arg.value)) {
                stack.push(inScope);
                convert(arg);
            } else {
                visitDeclaration(arg.declaration);
            }
        }

        override void visit(PostfixCallOperator arg) {
            auto callee = getBuiltValue(arg.lastChild());
            dispatch((OverloadSetValue overloadsValue) {
                ProcessedType[] argTypes = [];
                for (size_t i = 0; i < arg.children.length - 1; ++i) {
                    argTypes ~= typeProcessor.processExpression(arg.child(i));
                }
                auto resolver = OverloadResolver(mBuilder, overloadsValue.overloads, arg, argTypes);
                auto resolved = resolver.resolve();
                buildFunctionCall(resolved, arg.children[0 .. $ - 1]);
                convert(arg);
            }, (BuiltValue builtValue) {
                dispatch((ProcessedFunctionType asFunctionType) {
                    auto result = buildFunctionCall(asFunctionType,
                    builtValue.value, arg.children[0 .. $ - 1]);
                    pushTransient(asFunctionType.returnType, result);
                    convert(arg);
                })(builtValue.type);
            })(callee);
        }

        override void visit(TemplateInstantiation arg) {
            import abstrac.templates.instantiation;

            auto instantiator = TemplateInstantiator(mBuilder);
            auto result = instantiator.instantiate(arg);
            if (result.length != 1 || !result[0].complete) {
                stack.push(new OverloadSetValue(Overloads(result)));
                convert(arg);
                return;
            }
            visitDeclaration(instantiator.finalize(result));
        }

        override void visit(CharacterLiteral arg) {
            auto charType = ProcessedIntegerType.get(false, 8);
            auto charPtrType = ProcessedPointerType.get(charType);
            auto llvmType = LLVMArrayType(LLVMInt8Type(), cast(uint) arg.value.length + 1);
            auto glbstr = LLVMAddGlobal(mBuilder.module_, llvmType, "glbstr");
            LLVMSetInitializer(glbstr, LLVMConstString(toStringz(arg.value),
                    cast(uint) arg.value.length, 0));
            auto firstCharPtr = LLVMBuildGEP(builder, glbstr, [LLVMConstInt(LLVMInt8Type(),
                    0, 0), LLVMConstInt(LLVMInt8Type(), 0, 0)].ptr, 2, "");
            pushTransient(charPtrType, firstCharPtr);
            convert(arg);
        }

        override void visit(DecIntegerLiteral arg) {
            auto type = typeProcessor.processExpression(arg);
            auto llvmType = TypeBuilder(mBuilder).build(type, true);
            auto llvmValue = LLVMConstIntOfString(llvmType, toStringz(arg.value), 10);
            pushTransient(type, llvmValue);
            convert(arg);
        }

        override void visit(ProcessedValueNode arg) {
            pushTransient(arg.processedValue.getType(), fromProcessed(arg.processedValue));
            convert(arg);
        }

        override void visit(BuiltinExpression arg) {

            void invalidInstruction(Args...)(Args args) {
                assert(0);
            }

            void cmp(LLVMIntPredicate u, LLVMIntPredicate s, LLVMRealPredicate f)() {
                auto call = getUniqueChild!PostfixCallOperator(arg);
                assert(call.children.length == 2);
                auto lhs = getBuiltValue(call.child(0));
                auto rhs = getLLVMValue(call.child(1));
                dispatch((ProcessedIntegerType arg) {
                    if (arg.signed) {
                        auto result = LLVMBuildICmp(builder, s, lhs.value, rhs, "");
                        pushTransient(ProcessedIntegerType.get(false, 1), result);
                    } else {
                        auto result = LLVMBuildICmp(builder, u, lhs.value, rhs, "");
                        pushTransient(ProcessedIntegerType.get(false, 1), result);
                    }
                }, (ProcessedRealType arg) {
                    auto result = LLVMBuildFCmp(builder, f, lhs.value, rhs, "");
                    pushTransient(ProcessedIntegerType.get(false, 1), result);
                })(lhs.type);
                convert(arg);
            }

            void bitwise(alias buildFunction)() {
                auto call = getUniqueChild!PostfixCallOperator(arg);
                assert(call.children.length == 2);
                auto lhs = getBuiltValue(call.child(0));
                auto rhs = getLLVMValue(call.child(1));
                auto result = buildFunction(builder, lhs.value, rhs, "");
                pushTransient(lhs.type, result);
                convert(arg);
            }

            void typecast(alias buildFunction)() {
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                assert(instantiation.children.length == 1);
                auto targetTypeNode = getUniqueChild!Type(instantiation);
                auto targetType = typeProcessor.process(targetTypeNode);
                auto llvmType = TypeBuilder(mBuilder).build(targetType, true);
                auto call = getUniqueChild!PostfixCallOperator(arg);
                auto callArgument = getLLVMValue(onlyChild(call));
                pushTransient(targetType, buildFunction(builder, callArgument, llvmType, ""));
                convert(arg);
            }

            void arithmetic(alias uFn, alias sFn, alias fFn)() {
                auto call = getUniqueChild!PostfixCallOperator(arg);
                assert(call.children.length == 2);
                auto lhs = getBuiltValue(call.child(0));
                auto rhs = getLLVMValue(call.child(1));
                dispatch((ProcessedIntegerType arg) {
                    if (arg.signed) {
                        auto result = sFn(builder, lhs.value, rhs, "");
                        pushTransient(lhs.type, result);
                    } else {
                        auto result = uFn(builder, lhs.value, rhs, "");
                        pushTransient(lhs.type, result);
                    }
                }, (ProcessedRealType arg) {
                    auto result = fFn(builder, lhs.value, rhs, "");
                    pushTransient(lhs.type, result);
                })(lhs.type);
                convert(arg);
            }

            void alloca() {
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                assert(instantiation.children.length == 1);
                auto targetTypeNode = getUniqueChild!Type(instantiation);
                auto targetType = typeProcessor.process(targetTypeNode);
                auto llvmType = TypeBuilder(mBuilder).build(targetType, true);
                pushTransient(ProcessedPointerType.get(targetType),
                        LLVMBuildAlloca(builder, llvmType, ""));
                convert(arg);
            }

            void load() {
                auto call = getUniqueChild!PostfixCallOperator(arg);
                auto ptrval = getBuiltValue(onlyChild(call));
                dispatch((ProcessedPointerType ptrType) {
                    pushTransient(ptrType.pointedType, LLVMBuildLoad(builder, ptrval.value, ""));
                })(ptrval.type);
                convert(arg);
            }

            void store() {
                auto call = getUniqueChild!PostfixCallOperator(arg);
                assert(call.children.length == 2);
                auto lhs = getBuiltValue(call.child(0));
                auto rhs = getLLVMValue(call.child(1));
                dispatch((ProcessedPointerType _) {
                    pushTransient(ProcessedVoidType.get(), LLVMBuildStore(builder, rhs, lhs.value));
                })(lhs.type);
                convert(arg);
            }

            void gep() {
                auto call = getUniqueChild!PostfixCallOperator(arg);
                auto aggregate = getBuiltValue(call.child(0));
                auto indexNodes = call.children[1 .. $];
                assert(indexNodes.length > 0);
                LLVMValueRef[] indexValues = [];
                foreach (idxNode; indexNodes) {
                    import abstrac.codegen.evaluation : Evaluator;

                    // TODO: indexType shouldn't be restricted to 32bit
                    // (but is because struct indices must be i32)
                    indexValues ~= getLLVMValue(idxNode, ProcessedIntegerType.get(true, 32));
                }
                auto resultType = typeProcessor.processExpression(arg);
                pushTransient(resultType, LLVMBuildGEP(builder, aggregate.value,
                        indexValues.ptr, cast(uint) indexValues.length, ""));
                convert(arg);
            }

            void hasMetaData() {
                auto typeNodes = pathSelect!(TemplateInstantiation, Type)(arg);
                auto base = typeProcessor.process(typeNodes[0]);
                auto key = typeProcessor.process(typeNodes[1]);
                ulong result = base.hasMetaData(key) ? 1 : 0;
                pushTransient(ProcessedIntegerType.get(false, 1),
                        LLVMConstInt(LLVMInt1Type(), result, 0));
                convert(arg);
            }

            void getMetaData() {
                auto typeNodes = pathSelect!(TemplateInstantiation, Type)(arg);
                auto base = typeProcessor.process(typeNodes[0]);
                auto key = typeProcessor.process(typeNodes[1]);
                auto result = cast(ProcessedValue) base.getMetaData(key);
                assert(result !is null);
                pushTransient(result.getType(), fromProcessed(result));
                convert(arg);
            }

            final switch (arg.value) {
            case "cmp_eq":
                cmp!(LLVMIntEQ, LLVMIntEQ, LLVMRealOEQ)();
                break;
            case "cmp_ne":
                cmp!(LLVMIntNE, LLVMIntNE, LLVMRealONE)();
                break;
            case "cmp_gt":
                cmp!(LLVMIntUGT, LLVMIntSGT, LLVMRealOGT)();
                break;
            case "cmp_ge":
                cmp!(LLVMIntUGE, LLVMIntSGE, LLVMRealOGE)();
                break;
            case "cmp_lt":
                cmp!(LLVMIntULT, LLVMIntSLT, LLVMRealOLT)();
                break;
            case "cmp_le":
                cmp!(LLVMIntULE, LLVMIntSLE, LLVMRealOLE)();
                break;
            case "bitcast":
                typecast!LLVMBuildBitCast();
                break;
            case "trunc":
                typecast!LLVMBuildTrunc();
                break;
            case "fptrunc":
                typecast!LLVMBuildFPTrunc();
                break;
            case "signext":
                typecast!LLVMBuildSExt();
                break;
            case "zeroext":
                typecast!LLVMBuildZExt();
                break;
            case "fpext":
                typecast!LLVMBuildFPExt();
                break;
            case "fptoui":
                typecast!LLVMBuildFPToUI();
                break;
            case "fptosi":
                typecast!LLVMBuildFPToSI();
                break;
            case "uitofp":
                typecast!LLVMBuildUIToFP();
                break;
            case "sitofp":
                typecast!LLVMBuildSIToFP();
                break;
            case "ptrtoint":
                typecast!LLVMBuildPtrToInt();
                break;
            case "inttoptr":
                typecast!LLVMBuildIntToPtr();
                break;
            case "shl":
                bitwise!LLVMBuildShl();
                break;
            case "lshr":
                bitwise!LLVMBuildLShr();
                break;
            case "ashr":
                bitwise!LLVMBuildAShr();
                break;
            case "and":
                bitwise!LLVMBuildAnd();
                break;
            case "or":
                bitwise!LLVMBuildOr();
                break;
            case "xor":
                bitwise!LLVMBuildXor();
                break;
            case "add":
                arithmetic!(LLVMBuildAdd, LLVMBuildAdd, LLVMBuildFAdd)();
                break;
            case "sub":
                arithmetic!(LLVMBuildSub, LLVMBuildSub, LLVMBuildFSub)();
                break;
            case "mul":
                arithmetic!(LLVMBuildMul, LLVMBuildMul, LLVMBuildFMul)();
                break;
            case "div":
                arithmetic!(LLVMBuildUDiv, LLVMBuildSDiv, LLVMBuildFDiv)();
                break;
            case "rem":
                arithmetic!(LLVMBuildURem, LLVMBuildSRem, LLVMBuildFRem)();
                break;
            case "alloca":
                alloca();
                break;
            case "load":
                load();
                break;
            case "store":
                store();
                break;
            case "gep":
                gep();
                break;
            case "hasmetadata":
                hasMetaData();
                break;
            case "getmetadata":
                getMetaData();
                break;
            }
        }

        override void visit(Expression arg) {
            visitChildren(arg);
        }

        private LLVMValueRef buildFunctionCall(Nodes)(ProcessedFunctionType type,
                LLVMValueRef value, Nodes argNodes) {
            LLVMValueRef[] argValues = [];
            for (size_t i = 0; i < type.paramTypes.length; ++i) {
                argValues ~= getLLVMValue(argNodes[i], type.paramTypes[i]);
            }
            for (size_t i = type.paramTypes.length; i < argNodes.length; ++i) {
                argValues ~= getLLVMValue(argNodes[i]);
            }
            return LLVMBuildCall(builder, value, argValues.ptr, cast(uint) argValues.length, "");
        }

        private void buildFunctionCall(Nodes)(FunctionDeclaration declaration, Nodes argNodes) {
            ProcessedFunctionType type = typeProcessor.processDeclaration(declaration);
            LLVMValueRef function_ = mBuilder.requireFunction(declaration);
            auto result = buildFunctionCall(type, function_, argNodes);
            pushTransient(type.returnType, result);
        }

        private void pushTransient(ProcessedType type, LLVMValueRef value) {
            stack.push(new BuiltValue(type, value));
        }

        private ProcessedType popExpected() {
            return typeStack.pop();
        }

        private LLVMValueRef getLLVMValue(AAstNode arg) {
            return getLLVMValue(arg, null);
        }

        private LLVMValueRef getLLVMValue(AAstNode expression, ProcessedType type) {
            typeStack.push(type);
            return process(expression).value;
        }

        private ABuiltValue getBuiltValue(AAstNode arg) {
            return getBuiltValue(arg, null);
        }

        private ABuiltValue getBuiltValue(AAstNode arg, ProcessedType type) {
            typeStack.push(type);
            return process(arg);
        }

        private ABuiltValue process(AAstNode arg) {
            const beforeCount = stack.length;
            arg.accept(this);
            assert(stack.length == beforeCount + 1);
            return stack.pop();
        }

        private void convert(AAstNode arg) {
            import abstrac.templates.instantiation;
            import abstrac.codegen.conversion;

            auto expected = popExpected();
            if (expected is null) {
                return;
            }
            auto actual = stack.back().type;
            if (expected == actual) {
                return;
            }
            auto conversionPath = Conversion(mBuilder).find(actual, expected);
            if (conversionPath is null) {
                printAst(arg.parent.parent);
                assert(0,
                        "No known implicit conversion from " ~ actual.toString() ~ " to " ~ expected.toString());
            }
            LLVMValueRef topValue = stack.pop().value;
            foreach_reverse (function_; conversionPath) {
                auto functionValue = mBuilder.requireFunction(function_);
                topValue = LLVMBuildCall(builder, functionValue, [topValue].ptr, 1, "");
            }
            pushTransient(expected, topValue);
        }

        private void visitDeclaration(AAstNode arg) {
            import abstrac.ast.makers : makeOverloadSet;

            dispatch((TemplateInstance arg) { visitDeclaration(getEponym(arg)); },
                    (StaticSingleAssignmentStatement arg) {
                        auto inScope = scope_.getValue(arg.value);
                        assert(inScope !is null, arg.value ~ " is not in scope!");
                        stack.push(inScope);
                        convert(arg);
                    }, (FunctionDeclaration arg) {
                        auto type = typeProcessor.processDeclaration(arg);
                        auto value = mBuilder.requireFunction(arg);
                        pushTransient(type, value);
                        convert(arg);
                    }, (TemplateDeclaration arg) {
                        stack.push(new OverloadSetValue(Overloads(makeOverloadSet([arg]))));
                        convert(arg);
                    }, (OverloadSet arg) {
                        stack.push(new OverloadSetValue(Overloads(arg)));
                        convert(arg);
                    }, (PrecomputeDeclaration arg) {
                        import abstrac.codegen.evaluation : Evaluator;

                        auto expression = getUniqueChild!Expression(arg);
                        auto typeNode = getUniqueChild!Type(arg);
                        auto type = typeProcessor.process(typeNode);
                        auto value = Evaluator(mBuilder).evaluate(expression, type);
                        pushTransient(type, fromProcessed(value));
                        convert(arg);
                    })(arg);
        }

        private LLVMValueRef fromProcessed(ProcessedValue arg) {
            import abstrac.utils.multi_dispatch : dispatch;
            import std.conv : to;
            import std.string : toStringz;

            return dispatch((ProcessedIntegerValue arg) {
                return LLVMConstIntOfString(TypeBuilder(mBuilder)
                    .build(arg.type, true), toStringz(to!string(arg.value)), 10);
            }, (ProcessedFunctionValue arg) {
                return mBuilder.requireFunction(arg.declaration);
            }, (ProcessedStringValue arg) {
                import abstrac.ast.makers : makeStringLiteral;

                return getLLVMValue(makeStringLiteral(toStringz(arg.value)));
            })(arg);
        }

    }

}

private final class OverloadSetValue : WhiteHole!ABuiltValue {
    Overloads overloads;

    this(Overloads overloads) {
        this.overloads = overloads;
    }
}
