module abstrac.llvm.functions;

import abstrac.llvm.modules : ModuleBuilder, FunctionState;
import abstrac.llvm.values;
import abstrac.llvm.types;
import abstrac.codegen.evaluation : Evaluator;
import abstrac.utils.multi_dispatch : dispatch;
import abstrac.processed_values;
import abstrac.type_processor;
import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.utils.visitor : TVisitor;
import llvm;
import std.string : toStringz;

class FunctionBuilder {
    ModuleBuilder mBuilder;

    this(ModuleBuilder mBuilder) {
        this.mBuilder = mBuilder;
    }

    void build(FunctionState* state) {
        auto type = TypeBuilder(mBuilder).build(state.declaration);
        string name = state.declaration.value;
        state.value = LLVMAddFunction(mBuilder.module_, toStringz(name), type);
        FunctionBody functionBody = tryGetUniqueChild!FunctionBody(state.declaration);
        if (functionBody !is null) {
            FunctionParameter[] params = pathSelect!FunctionParameter(state.declaration);
            foreach (i, param; params) {
                auto acType = TypeProcessor(mBuilder).process(getUniqueChild!Type(param));
                if (param.value != "") {
                    state.scope_.addParameter(param.value, acType,
                            LLVMGetParam(state.value, cast(uint) i));
                }
            }
            Visitor visitor = new Visitor(state);
            functionBody.accept(visitor);
        }
    }

    private class Visitor : TVisitor!true {
        import abstrac.utils.stack : Stack;

        FunctionState* state;
        LLVMBuilderRef builder;
        ValueScope currentScope;
        size_t controlFlowCounter;

        struct BlockState {
            ScopeGuardStatement[] scopeGuards;
            bool returned;
        }

        Stack!BlockState blockStateStack;

        this(FunctionState* state) {
            this.state = state;
            currentScope = state.scope_;
        }

        private void pushBlockState() {
            currentScope = new ValueScope(currentScope);
            blockStateStack.push(BlockState());
        }

        private BlockState popBlockState() {
            if (!blockState().returned) {
                visitScopeGuards();
            }
            currentScope = currentScope.parent;
            return blockStateStack.pop();
        }

        private ref BlockState blockState() {
            return blockStateStack.back();
        }

        private void returnBlock() {
            blockStateStack.back().returned = true;
        }

        private void visitScopeGuards() {
            foreach_reverse (blockState; blockStateStack.array) {
                foreach_reverse (scopeGuard; blockState.scopeGuards) {
                    foreach (child; scopeGuard.children) {
                        child.accept(this);
                    }
                }
            }
        }

        private ABuiltValue buildValue(AAstNode arg, ProcessedType type) {
            return mBuilder.buildValue(arg, builder, currentScope, type);
        }

        private bool isVoidFunction() {
            import abstrac.processed_types : ProcessedVoidType,
                ProcessedFunctionType, ProcessedType;

            ProcessedFunctionType functionType = TypeProcessor(mBuilder).processDeclaration(
                    state.declaration);
            ProcessedType returnType = functionType.returnType;
            return returnType == ProcessedVoidType.get();
        }

        private @property string functionName() {
            return state.declaration.value;
        }

        override void visit(FunctionBody arg) {
            LLVMBasicBlockRef entryBlock = LLVMAppendBasicBlock(state.value, "entry");
            builder = LLVMCreateBuilder();
            LLVMPositionBuilderAtEnd(builder, entryBlock);
            pushBlockState();
            visitChildren(arg);
            if (!popBlockState().returned) {
                if (isVoidFunction()) {
                    LLVMBuildRetVoid(builder);
                } else {
                    assert(0, "Not all code paths of '" ~ functionName ~ "' return a value!");
                }
            }
            LLVMDisposeBuilder(builder);
        }

        override void visit(Statement arg) {
            assert(!blockStateStack.back().returned, "Unreachable code!");
            visitChildren(arg);
        }

        override void visit(StaticSingleAssignmentStatement arg) {
            auto builtValue = buildValue(onlyChild(arg), null);
            currentScope.addSSA(arg.value, builtValue.type, builtValue.value);
        }

        override void visit(StaticAssertStatement arg) {
            auto expr = arg.child(0);
            auto eval = Evaluator(mBuilder).evaluate(expr, ProcessedIntegerType.get(false, 1));
            dispatch((ProcessedIntegerValue val) {
                assert(val.value != 0, "Static assertion failed!");
            })(eval);
        }

        override void visit(StaticIfStatement arg) {
            auto expr = arg.child(0);
            auto eval = Evaluator(mBuilder).evaluate(expr, ProcessedIntegerType.get(false, 1));
            dispatch((ProcessedIntegerValue val) {
                if (val.value != 0) {
                    getUniqueChild!StaticIfStatementThenBlock(arg).accept(this);
                } else if (auto elseBlock = tryGetUniqueChild!StaticIfStatementElseBlock(arg)) {
                    elseBlock.accept(this);
                }
            })(eval);
        }

        override void visit(StaticIfStatementThenBlock arg) {
            visitChildren(arg);
        }

        override void visit(StaticIfStatementElseBlock arg) {
            visitChildren(arg);
        }

        override void visit(ExpressionStatement arg) {
            mBuilder.buildValue(arg.child(0), builder, currentScope, null);
        }

        override void visit(ReturnStatement arg) {
            if (arg.children.length == 0) {
                visitScopeGuards();
                returnBlock();
                LLVMBuildRetVoid(builder);
            } else {
                auto functionType = TypeProcessor(mBuilder).processDeclaration(state.declaration);
                auto value = buildValue(onlyChild(arg), functionType.returnType).value;
                visitScopeGuards();
                returnBlock();
                LLVMBuildRet(builder, value);
            }
        }

        private LLVMBasicBlockRef appendBasicBlock(string prefix, bool incrementSuffix = false) {
            import std.conv : to;

            if (incrementSuffix) {
                ++controlFlowCounter;
            }
            return LLVMAppendBasicBlock(state.value,
                    toStringz(prefix ~ to!string(controlFlowCounter)));
        }

        override void visit(IfStatement arg) {
            Expression condition = getUniqueChild!Expression(arg);
            LLVMValueRef condValue = buildValue(condition, ProcessedIntegerType.get(false, 1))
                .value;
            IfThenBlock thenNode = getUniqueChild!IfThenBlock(arg);
            IfElseBlock elseNode = tryGetUniqueChild!IfElseBlock(arg);
            LLVMBasicBlockRef ifBlock = appendBasicBlock("ifthen", true);
            LLVMBasicBlockRef elseBlock = elseNode is null ? null : appendBasicBlock("ifelse");
            LLVMBasicBlockRef endBlock = elseNode is null ? appendBasicBlock("ifend") : null;
            LLVMBuildCondBr(builder, condValue, ifBlock, elseBlock is null ? endBlock : elseBlock);
            LLVMPositionBuilderAtEnd(builder, ifBlock);
            pushBlockState();
            thenNode.accept(this);
            if (!popBlockState().returned) {
                if (endBlock is null) {
                    endBlock = appendBasicBlock("ifend");
                }
                LLVMBuildBr(builder, endBlock);
            }
            if (elseNode !is null) {
                LLVMPositionBuilderAtEnd(builder, elseBlock);
                pushBlockState();
                elseNode.accept(this);
                if (!popBlockState().returned) {
                    if (endBlock is null) {
                        endBlock = appendBasicBlock("ifend");
                    }
                    LLVMBuildBr(builder, endBlock);
                }
            }
            if (endBlock !is null) {
                LLVMPositionBuilderAtEnd(builder, endBlock);
            } else {
                returnBlock();
            }
        }

        override void visit(IfThenBlock arg) {
            visitChildren(arg);
        }

        override void visit(IfElseBlock arg) {
            visitChildren(arg);
        }

        override void visit(WhileLoopStatement arg) {
            WhileLoopBody loopBody = getUniqueChild!WhileLoopBody(arg);
            WhileLoopCondition condition = getUniqueChild!WhileLoopCondition(arg);
            LLVMBasicBlockRef conditionBlock = appendBasicBlock("whilecond", true);
            LLVMBasicBlockRef bodyBlock = appendBasicBlock("whileloop");
            LLVMBasicBlockRef endBlock = appendBasicBlock("whileend");
            LLVMBuildBr(builder, conditionBlock);
            LLVMPositionBuilderAtEnd(builder, conditionBlock);
            LLVMValueRef condValue = buildValue(onlyChild(condition),
                    ProcessedIntegerType.get(false, 1)).value;
            LLVMBuildCondBr(builder, condValue, bodyBlock, endBlock);
            LLVMPositionBuilderAtEnd(builder, bodyBlock);
            pushBlockState();
            loopBody.accept(this);
            popBlockState();
            LLVMBuildBr(builder, conditionBlock);
            LLVMPositionBuilderAtEnd(builder, endBlock);
        }

        override void visit(WhileLoopBody arg) {
            visitChildren(arg);
        }

        override void visit(ScopeGuardStatement arg) {
            blockStateStack.back().scopeGuards ~= arg;
        }

    }

}
