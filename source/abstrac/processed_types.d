module abstrac.processed_types;

import abstrac.processed_values;
import std.container.array : Array;

abstract class ProcessedElement {
    abstract override string toString() const;
    abstract override bool opEquals(Object other);
    abstract override size_t toHash();
}

abstract class ProcessedType : ProcessedElement {
    Array!ProcessedType modifiers;
    ProcessedValue[ProcessedType] metadata;

    ProcessedType setMetaData(ProcessedType key, ProcessedValue value) {
        assert(key !in metadata);
        auto copy = clone();
        copy.metadata[key] = value;
        return copy;
    }

    ProcessedType unsetMetaData(ProcessedType key) {
        if (key in metadata) {
            auto copy = clone();
            copy.metadata.remove(key);
            return copy;
        }
        return null;
    }

    bool hasMetaData(ProcessedType key) {
        return (key in metadata) !is null;
    }

    ProcessedValue getMetaData(ProcessedType key) {
        if (auto entry = key in metadata) {
            return *entry;
        }
        throw new Exception(toString() ~ " does not have metadata of " ~ key.toString());
    }

    ProcessedType setModifier(ProcessedType modifier) {
        import std.algorithm : canFind;

        if (!modifiers[].canFind(modifier)) {
            auto copy = clone();
            copy.modifiers ~= modifier;
            return copy;
        }
        return this;
    }

    ProcessedType unsetModifier(ProcessedType modifier) {
        import std.algorithm : canFind, filter;

        if (!modifiers[].canFind(modifier)) {
            return this;
        }
        auto copy = clone();
        copy.modifiers = Array!ProcessedType(modifiers[].filter!(a => a != modifier));
        return copy;
    }

    abstract ProcessedType clone();
}

class ProcessedVoidType : ProcessedType {
    mixin ProcessedTypeMixin;
}

class ProcessedIntegerType : ProcessedType {
    bool signed;
    uint bitWidth;

    mixin ProcessedTypeMixin!("signed", "bitWidth");
}

class ProcessedRealType : ProcessedType {
    uint bitWidth;

    mixin ProcessedTypeMixin!("bitWidth");
}

class ProcessedPointerType : ProcessedType {
    ProcessedType pointedType;

    mixin ProcessedTypeMixin!("pointedType");
}

class ProcessedArrayType : ProcessedType {
    size_t size;
    ProcessedType elementType;

    mixin ProcessedTypeMixin!("size", "elementType");
}

class ProcessedTupleType : ProcessedType {
    ProcessedType[] memberTypes;

    mixin ProcessedTypeMixin!("memberTypes");
}

class ProcessedStructType : ProcessedType {
    import abstrac.ast : StructDeclaration;

    StructDeclaration declaration;
    ProcessedType[] memberTypes;
    mixin ProcessedTypeMixin!("declaration", "memberTypes");
}

class ProcessedFunctionType : ProcessedType {
    ProcessedType returnType;
    ProcessedType[] paramTypes;
    bool variadic;
    mixin ProcessedTypeMixin!("returnType", "paramTypes", "variadic");
}

class ProcessedImplicitToMDKType : ProcessedType {
    ProcessedType targetType;
    mixin ProcessedTypeMixin!("targetType");
}

class ProcessedImplicitFromMDKType : ProcessedType {
    ProcessedType sourceType;
    mixin ProcessedTypeMixin!("sourceType");
}

class ProcessedLiteralModType : ProcessedType {
    mixin ProcessedTypeMixin;
}

private mixin template ProcessedTypeMixin(memberNames...) {

    protected this() {
    }

    static typeof(this) get(Args...)(Args args) {
        static assert(Args.length == memberNames.length);
        typeof(this) result = new typeof(this);
        static foreach (i, memberName; memberNames) {
            {
                auto memptr = &__traits(getMember, result, memberName); // @suppress(dscanner.suspicious.unused_variable)
                static assert(is(Args[i] : typeof(*memptr)));
                *memptr = args[i];
            }
        }
        return result;
    }

    override ProcessedType clone() {
        typeof(this) result = new typeof(this);
        static foreach (i, memberName; memberNames) {
            {
                auto thismemptr = &__traits(getMember, this, memberName);
                auto othermemptr = &__traits(getMember, result, memberName); // @suppress(dscanner.suspicious.unused_variable)
                *othermemptr = *thismemptr;
            }
        }
        result.modifiers = modifiers.dup;
        result.metadata = metadata.dup;
        return result;
    }

    override bool opEquals(Object other) {
        auto asThisType = cast(typeof(this)) other;
        if (asThisType is null) {
            return false;
        }
        if (asThisType.modifiers != this.modifiers) {
            return false;
        }
        static foreach (memberName; memberNames) {
            if (__traits(getMember, this, memberName) != __traits(getMember, asThisType, memberName)) {
                return false;
            }
        }
        return true;
    }

    override string toString() const {
        import std.range : repeat;
        import std.conv : to;

        static size_t __indent = 0;
        string indented(string val) {
            return to!string(' '.repeat(__indent)) ~ val;
        }

        string result = __traits(identifier, typeof(this)) ~ " {\n";
        __indent += 2;
        foreach (modifier; modifiers) {
            result ~= indented(modifier.toString() ~ "\n");
        }
        static foreach (memberName; memberNames) {
            result ~= indented(memberName ~ ": " ~ to!string(__traits(getMember,
                    this, memberName)) ~ "\n");
        }
        __indent -= 2;
        result ~= indented("}");
        return result;
    }

    override size_t toHash() @trusted {
        import std.traits : fullyQualifiedName;

        size_t result = hashOf(fullyQualifiedName!(typeof(this)));
        foreach (modifier; modifiers) {
            result *= modifier.toHash() + 78_433;
        }
        static foreach (memberName; memberNames) {
            result *= hashOf(__traits(getMember, this, memberName)) + 78_433;
        }
        return result;
    }
}
