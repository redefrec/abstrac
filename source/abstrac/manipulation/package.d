module abstrac.manipulation;

import abstrac.manipulation.restructure : RestructureManipulator;
import abstrac.manipulation.identifier_bind : IdentifierBindManipulator;
import abstrac.utils.visitor : TVisitor, IVisitor;
import abstrac.ast;
import std.container : BinaryHeap, Array;
import std.meta : AliasSeq;

void applyManipulations(Unit[string] units) {
    import abstrac.manipulation.custom;

    struct ManipulationStep {
        Unit unit;
        IManipulator manipulator;
    }
    BinaryHeap!(Array!ManipulationStep, "a.manipulator.order > b.manipulator.order") steps;
    CustomManipulatorSet[] customs = [];
    foreach (id, unit; units) {
        CustomManipulatorSet customManipulators = CustomManipulatorSet(unit);
        customs ~= customManipulators;
        static foreach (M; AliasSeq!(RestructureManipulator, IdentifierBindManipulator)) {
            {
                M manipulator = new M;
                manipulator.allUnits = units;
                steps.insert(ManipulationStep(unit, manipulator));
            }
        }
        foreach (customManipulator; customManipulators.manipulators) {
            steps.insert(ManipulationStep(unit, customManipulator));
        }
    }
    while (!steps.empty()) {
        auto step = steps.front();
        step.unit.accept(step.manipulator);
        steps.removeFront();
    }
    foreach (custom; customs) {
        custom.cleanup();
    }
}

interface IManipulator : IVisitor {
    long order() const;
}

abstract class TManipulator(bool restricted) : TVisitor!restricted, IManipulator {
    private long _order;
    protected Unit[string] allUnits;

    this(long order) {
        this._order = order;
    }

    override long order() const {
        return _order;
    }

    protected final Unit getUnit(string name) {
        auto lookup = name in allUnits;
        assert(lookup !is null);
        return *lookup;
    }
}
