module abstrac.manipulation.custom.builder;

import abstrac.ast;
import abstrac.utils.visitor : TVisitor;
import abstrac.ast.makers, abstrac.ast.helpers;
import llvm;

class VisitBuilder {
    LLVMModuleRef module_;
    LLVMExecutionEngineRef jit;
    size_t counter;
    LLVMTypeRef[string] types;
    const(char)*[ExtensionManipulationVisit] functionNames;

    this(LLVMModuleRef module_, LLVMExecutionEngineRef jit) {
        this.module_ = module_;
        this.jit = jit;
    }

    void buildVisitFunction(ExtensionManipulationVisit arg) {
        import abstrac.manipulation.restructure : RestructureManipulator;
        import std.string : toStringz;
        import std.conv : to;

        arg.accept(new RestructureManipulator);
        auto functionName = toStringz("visit" ~ to!string(counter++));
        auto paramType = LLVMPointerType(LLVMInt8Type(), 0);
        auto functionType = LLVMFunctionType(LLVMVoidType(), &paramType, 1, 0);
        auto functionValue = LLVMAddFunction(module_, functionName, functionType);
        arg.child(0).accept(new VisitBuildVisitor(functionValue));
        functionNames[arg] = functionName;
    }

    ulong getJitted(ExtensionManipulationVisit arg) {
        return LLVMGetFunctionAddress(jit, functionNames[arg]);
    }

    private class VisitBuildVisitor : TVisitor!true {
        import abstrac.utils.stack : Stack;

        LLVMValueRef function_;
        LLVMBuilderRef builder;
        Scope scope_;
        Stack!LLVMValueRef stack;
        Stack!LLVMTypeRef typeStack;

        this(LLVMValueRef function_) {
            this.function_ = function_;
        }

        override void visit(Type arg) {
            visitChildren(arg);
        }

        override void visit(ExpressionStatement arg) {
            visitChildren(arg);
        }

        override void visit(Expression arg) {
            visitChildren(arg);
        }

        override void visit(Statement arg) {
            visitChildren(arg);
        }

        override void visit(BuiltinType arg) {
            final switch (arg.value) {
            case "long":
                typeStack.push(hostToLLVMType!long());
                break;
            case "node":
                typeStack.push(hostToLLVMType!AAstNode());
                break;
            }
        }

        override void visit(FunctionBody arg) {
            auto entry = LLVMAppendBasicBlock(function_, "entry");
            builder = LLVMCreateBuilder();
            LLVMPositionBuilderAtEnd(builder, entry);
            scope_ = new Scope(null);
            scope_.addValue("this", LLVMGetParam(function_, 0));
            visitChildren(arg);
            scope_ = scope_.parent;
            LLVMBuildRetVoid(builder);
            LLVMVerifyFunction(function_, LLVMPrintMessageAction);
            LLVMDisposeBuilder(builder);
        }

        override void visit(StaticSingleAssignmentStatement arg) {
            onlyChild(arg).accept(this);
            scope_.addValue(arg.value, stack.pop());
        }

        override void visit(PostfixCallOperator arg) {
            arg.lastChild().accept(this);
            auto callee = stack.pop();
            LLVMValueRef[] args = [];
            foreach (child; arg.children[0 .. $ - 1]) {
                child.accept(this);
                args ~= stack.pop();
            }

            stack.push(LLVMBuildCall(builder, callee, args.ptr, cast(uint) args.length, ""));
        }

        override void visit(CharacterLiteral arg) {
            import std.string : toStringz;

            auto llvmType = LLVMArrayType(LLVMInt8Type(), cast(uint) arg.value.length + 1);
            auto glbstr = LLVMAddGlobal(module_, llvmType, "glbstr");
            LLVMSetInitializer(glbstr, LLVMConstString(toStringz(arg.value),
                    cast(uint) arg.value.length, 0));
            auto firstCharPtr = LLVMBuildGEP(builder, glbstr, [LLVMConstInt(LLVMInt8Type(),
                    0, 0), LLVMConstInt(LLVMInt8Type(), 0, 0)].ptr, 2, "");
            stack.push(firstCharPtr);
        }

        override void visit(DecIntegerLiteral arg) {
            import std.string : toStringz;

            stack.push(LLVMConstIntOfString(LLVMInt64Type(), toStringz(arg.value), 10));
        }

        private void pushScope() {
            scope_ = new Scope(scope_);
        }

        private void popScope() {
            scope_ = scope_.parent;
        }

        private size_t controlFlowCounter = 0;
        private LLVMBasicBlockRef appendBasicBlock(string prefix, bool incrementSuffix = false) {
            import std.conv : to;
            import std.string : toStringz;

            return LLVMAppendBasicBlock(function_, toStringz(prefix ~ to!string(incrementSuffix
                    ? ++controlFlowCounter : controlFlowCounter)));
        }

        override void visit(IfStatement arg) {
            Expression condition = getUniqueChild!Expression(arg);
            condition.accept(this);
            LLVMValueRef condValue = stack.pop();
            IfThenBlock thenNode = getUniqueChild!IfThenBlock(arg);
            IfElseBlock elseNode = tryGetUniqueChild!IfElseBlock(arg);
            LLVMBasicBlockRef ifBlock = appendBasicBlock("ifthen", true);
            LLVMBasicBlockRef elseBlock = elseNode is null ? null : appendBasicBlock("ifelse");
            LLVMBasicBlockRef endBlock = appendBasicBlock("ifend");
            LLVMBuildCondBr(builder, condValue, ifBlock, elseBlock is null ? endBlock : elseBlock);
            LLVMPositionBuilderAtEnd(builder, ifBlock);
            pushScope();
            thenNode.accept(this);
            popScope();
            LLVMBuildBr(builder, endBlock);
            if (elseNode !is null) {
                LLVMPositionBuilderAtEnd(builder, elseBlock);
                pushScope();
                elseNode.accept(this);
                popScope();
                LLVMBuildBr(builder, endBlock);
            }
            LLVMPositionBuilderAtEnd(builder, endBlock);
        }

        override void visit(IfThenBlock arg) {
            visitChildren(arg);
        }

        override void visit(IfElseBlock arg) {
            visitChildren(arg);
        }

        override void visit(WhileLoopStatement arg) {
            WhileLoopBody loopBody = getUniqueChild!WhileLoopBody(arg);
            WhileLoopCondition condition = getUniqueChild!WhileLoopCondition(arg);
            LLVMBasicBlockRef conditionBlock = appendBasicBlock("whilecond", true);
            LLVMBasicBlockRef bodyBlock = appendBasicBlock("whileloop");
            LLVMBasicBlockRef endBlock = appendBasicBlock("whileend");
            LLVMBuildBr(builder, conditionBlock);
            LLVMPositionBuilderAtEnd(builder, conditionBlock);
            onlyChild(condition).accept(this);
            LLVMValueRef condValue = stack.pop();
            LLVMBuildCondBr(builder, condValue, bodyBlock, endBlock);
            LLVMPositionBuilderAtEnd(builder, bodyBlock);
            pushScope();
            loopBody.accept(this);
            popScope();
            LLVMBuildBr(builder, conditionBlock);
            LLVMPositionBuilderAtEnd(builder, endBlock);
        }

        override void visit(WhileLoopBody arg) {
            visitChildren(arg);
        }

        override void visit(BuiltinExpression arg) {
            import std.meta : AliasSeq;
            import std.traits : Parameters;

            alias BuiltinMap = AliasSeq!("fneg", LLVMBuildFNeg, "add",
                    LLVMBuildAdd, "fadd", LLVMBuildFAdd, "sub", LLVMBuildSub,
                    "fsub", LLVMBuildFSub, "mul", LLVMBuildMul, "fmul",
                    LLVMBuildFMul, "udiv", LLVMBuildUDiv, "sdiv", LLVMBuildSDiv,
                    "fdiv", LLVMBuildFDiv, "urem", LLVMBuildURem, "srem",
                    LLVMBuildSRem, "frem", LLVMBuildFRem, "shl", LLVMBuildShl,
                    "lshr", LLVMBuildLShr, "ashr", LLVMBuildAShr, "and", LLVMBuildAnd,
                    "or", LLVMBuildOr, "xor", LLVMBuildXor, "load", LLVMBuildLoad);
        lswitch:
            final switch (arg.value) {
                static foreach (i; 0 .. (BuiltinMap.length / 2)) {
            case BuiltinMap[i * 2]:
                    Parameters!(BuiltinMap[i * 2 + 1]) arguments;
                    arguments[0] = builder;
                    arguments[$ - 1] = "";
                    auto callOperator = getUniqueChild!PostfixCallOperator(arg);
                    static foreach (j; 1 .. (arguments.length - 1)) {
                        callOperator.child(j - 1).accept(this);
                        arguments[j] = stack.pop();
                    }
                    stack.push(BuiltinMap[i * 2 + 1](arguments));
                    break lswitch;
                }
            case "alloca":
                auto type = pathSelect!(TemplateInstantiation, Type)(arg)[0];
                type.accept(this);
                stack.push(LLVMBuildAlloca(builder, typeStack.pop(), ""));
                break;
            case "store":
                auto callOperator = getUniqueChild!PostfixCallOperator(arg);
                callOperator.child(1).accept(this);
                callOperator.child(0).accept(this);
                auto left = stack.pop();
                auto right = stack.pop();
                LLVMBuildStore(builder, left, right);
                break;
            case "cmp_lt":
                auto callOperator = getUniqueChild!PostfixCallOperator(arg);
                callOperator.child(1).accept(this);
                callOperator.child(0).accept(this);
                auto left = stack.pop();
                auto right = stack.pop();
                stack.push(LLVMBuildICmp(builder, LLVMIntSLT, left, right, ""));
                break;
            case "cmp_eq":
                auto callOperator = getUniqueChild!PostfixCallOperator(arg);
                callOperator.child(1).accept(this);
                callOperator.child(0).accept(this);
                stack.push(LLVMBuildICmp(builder, LLVMIntEQ, stack.pop(), stack.pop(), ""));
                break;
            }
        }

        override void visit(Identifier arg) {
            import std.traits, std.meta;

            LLVMTypeRef ptrIntType;
            static if (AAstNode.sizeof == 4) {
                ptrIntType = LLVMInt32Type();
            } else static if (AAstNode.sizeof == 8) {
                ptrIntType = LLVMInt64Type();
            } else {
                static assert(0);
            }
            if (auto lookup = scope_.lookup(arg.value)) {
                stack.push(lookup);
                return;
            }
            switch (arg.value) {
                static foreach (hostFunction; AliasSeq!(getSymbolsByUDA!(abstrac.ast.makers,
                        "extension_usable"), getSymbolsByUDA!(abstrac.ast.helpers,
                        "extension_usable"))) {
            case __traits(identifier, hostFunction): {
                        LLVMTypeRef[] paramTypes = [];
                        static foreach (ParamT; Parameters!hostFunction) {
                            paramTypes ~= hostToLLVMType!ParamT();
                        }
                        LLVMTypeRef returnType = hostToLLVMType!(ReturnType!hostFunction);
                        LLVMTypeRef functionType = LLVMFunctionType(returnType,
                                paramTypes.ptr, cast(uint) paramTypes.length, 0);
                        stack.push(LLVMConstIntToPtr(LLVMConstInt(ptrIntType,
                                cast(ulong)&hostFunction, 0), LLVMPointerType(functionType, 0)));
                        return;
                    }
                }
            default:
                break;
            }
            alias prefixTemplates = AliasSeq!(Alias!"new_", makeNewNode, Alias!"tryGet_", tryGetUniqueChild,
                    Alias!"get_", getUniqueChild, Alias!"getAll_", extensionGetAll);
            static foreach (i; 0 .. (prefixTemplates.length / 2)) {
                {
                    alias prefix = prefixTemplates[i * 2];
                    alias template_ = prefixTemplates[i * 2 + 1];
                    if (arg.value[0 .. (prefix.length)] == prefix) {
                        final switch (arg.value[prefix.length .. $]) {
                            static foreach (Node; Nodes.Language) {
                        case __traits(identifier, Node): {
                                    LLVMTypeRef returnType = hostToLLVMType!(
                                            ReturnType!(template_!Node))();
                                    LLVMTypeRef[] paramTypes = [];
                                    static foreach (ParamT; Parameters!(template_!Node)) {
                                        paramTypes ~= hostToLLVMType!ParamT();
                                    }
                                    LLVMTypeRef funType = LLVMFunctionType(returnType,
                                            paramTypes.ptr, cast(uint) paramTypes.length, 0);
                                    stack.push(LLVMConstIntToPtr(LLVMConstInt(ptrIntType,
                                            cast(ulong)&template_!Node, 0),
                                            LLVMPointerType(funType, 0)));
                                    return;
                                }
                            }
                        }
                    }
                }

            }
            assert(0, "Unrecognized value: " ~ arg.value);
        }

        private LLVMTypeRef hostToLLVMType(T : void)() {
            return LLVMVoidType();
        }

        private LLVMTypeRef hostToLLVMType(T : long)() {
            return LLVMInt64Type();
        }

        private LLVMTypeRef hostToLLVMType(T : bool)() {
            return LLVMInt1Type();
        }

        private LLVMTypeRef hostToLLVMType(T : const(char)*)() {
            return LLVMPointerType(LLVMInt8Type(), 0);
        }

        private LLVMTypeRef hostToLLVMType(T)() if (is(T : AAstNode)) {
            return LLVMPointerType(LLVMInt8Type(), 0);
        }

        private LLVMTypeRef hostToLLVMType(T : AAstNode[])() {
            return LLVMIntType(T.sizeof * 8);
        }

    }
}

private extern (C) T makeNewNode(T)() {
    return new T;
}

private class Scope {
    LLVMValueRef[string] values;
    Scope parent;

    this(Scope parent) {
        this.parent = parent;
    }

    void addValue(string name, LLVMValueRef value) {
        assert(name !in values);
        values[name] = value;
    }

    LLVMValueRef lookup(string name) {
        if (name in values) {
            return values[name];
        } else if (parent !is null) {
            return parent.lookup(name);
        } else {
            return null;
        }

    }

}
