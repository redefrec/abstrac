module abstrac.manipulation.custom.manipulator;

import abstrac.manipulation : TManipulator;
import abstrac.ast;
import std.meta;
import std.container.array;
import core.memory : GC;

package class Manipulator : TManipulator!false {
    staticMap!(VisitArrayT, NonCustom) visitFunctions;
    VisitArrayT!CustomAstNode[string] customVisitFunctions;

    this(long _order) {
        super(_order);
    }

    static foreach (N; NonCustom) {
        override void visit(N arg) {
            visitChildren(arg);
            foreach (function_; visitFunctions[Index!N]) {
                GC.disable();
                function_(arg);
                GC.enable();
            }
        }
    }

    override void visit(CustomAstNode arg) {
        visitChildren(arg);
        if (auto visits = arg.name in customVisitFunctions) {
            foreach (function_; *visits) {
                GC.disable();
                function_(arg);
                GC.enable();
            }
        }
    }

    void addFunction(N)(VisitT!N function_) {
        visitFunctions[Index!N] ~= function_;
    }

    void addFunction(string nodeName, VisitT!CustomAstNode function_) {
        auto array = nodeName in customVisitFunctions;
        if (array is null) {
            array = &(customVisitFunctions[nodeName] = []);
        }
        *array ~= function_;
    }
}

private alias NonCustom = Erase!(CustomAstNode, Nodes.Language);
private alias VisitT(N) = extern (C) void function(N);
private alias VisitArrayT(N) = VisitT!N[];
private enum Index(N) = staticIndexOf!(N, NonCustom);
