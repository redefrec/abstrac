module abstrac.manipulation.custom;

import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.ast.makers;
import abstrac.utils.visitor : TVisitor;
import abstrac.manipulation : IManipulator;
import abstrac.manipulation.custom.builder;
import llvm;
import std.container.array : Array;

struct CustomManipulatorSet {
    private LLVMModuleRef module_;
    private LLVMExecutionEngineRef jit;
    private Unit unit;
    IManipulator[] manipulators;

    @disable this();

    this(Unit unit) {
        this.unit = unit;
        module_ = LLVMModuleCreateWithName("custom_manipulators");
        char* message = null;
        assert(!LLVMCreateMCJITCompilerForModule(&jit, module_, null, 0, &message));
        buildManipulators();
    }

    private void buildManipulators() {
        import abstrac.manipulation.custom.manipulator;

        auto builder = new VisitBuilder(module_, jit);
        auto visitsMap = preloadVisits();
        foreach (order, visits; visitsMap) {
            foreach (visit; visits) {
                builder.buildVisitFunction(visit);
            }
        }
        foreach (order, visits; visitsMap) {
            Manipulator manipulator = new Manipulator(order);
            foreach (visit; visits) {
                string nodeName = visit.value;
            lswitch:
                switch (nodeName) {
                    import std.meta;

                    static foreach (Node; Erase!(CustomAstNode, Nodes.Language)) {
                case __traits(identifier, Node): {
                            alias VisitFunctionT = extern (C) void function(Node);
                            auto function_ = cast(VisitFunctionT) builder.getJitted(visit);
                            manipulator.addFunction!Node(function_);
                            break lswitch;
                        }
                    }
                default: {
                        alias VisitFunctionT = extern (C) void function(CustomAstNode);
                        auto function_ = cast(VisitFunctionT) builder.getJitted(visit);
                        manipulator.addFunction(nodeName, function_);
                        break lswitch;
                    }
                }
            }
            manipulators ~= manipulator;
        }
    }

    private alias VisitArray = Array!ExtensionManipulationVisit;

    private auto preloadVisits() {
        VisitArray[long] visits;
        visits[0] = VisitArray();
        visits.remove(0);
        foreach (extension; pathSelect!Extension(unit)) {
            preloadVisits(extension, visits);
        }
        foreach (import_; unit.imports) {
            foreach (extension; pathSelect!Extension(import_.prelim)) {
                preloadVisits(extension, visits);
            }
        }
        return visits;
    }

    private void preloadVisits(Extension extension, VisitArray[long] visits) {
        auto manipulations = pathSelect!ExtensionManipulationProperty(extension);
        foreach (manipulation; manipulations) {
            auto order = parseLongValue(manipulation.child(0));
            auto array = order in visits;
            if (array is null) {
                array = &(visits[order] = VisitArray());
            }
            foreach (visit; pathSelect!ExtensionManipulationVisit(manipulation)) {
                *array ~= visit;
            }
        }
    }

    void cleanup() {
        LLVMDisposeExecutionEngine(jit);
    }
}
