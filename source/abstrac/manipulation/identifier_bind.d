module abstrac.manipulation.identifier_bind;

import abstrac.manipulation : TManipulator;
import abstrac.ast;
import abstrac.ast.helpers;
import std.meta;

class IdentifierBindManipulator : TManipulator!false {

    enum Order = 10_000;

    this() {
        super(Order);
    }

    override void visit(Unit arg) {
        scope_ = new Scope(arg, scope_);
        visitChildren(arg);
        scope_.bindIdentifiers();
        scope_ = null;
    }

    override void visit(Extension) {
    }

    static foreach (Node; RelevantNodes) {
        override void visit(Node arg) {
            static if (isDeclaration!Node) {
                assert(scope_ !is null);
                scope_.addDeclaration(arg);
            }
            static if (isScope!Node) {
                scope_ = new Scope(arg, scope_);
                scope (exit) {
                    scope_ = scope_.parent;
                }
            }
            visitChildren(arg);
        }
    }

    override void visit(Identifier arg) {
        assert(scope_ !is null);
        scope_.addIdentifier(arg);
    }

    override void visit(UnitImport arg) {
        import abstrac.utils.visitor : TVisitor;

        auto unit = getUnit(arg.value);

        class ImportVisitor : TVisitor!false {
            override void visit(Unit importedUnit) {
                visitChildren(importedUnit);
            }

            static foreach (Node; RelevantNodes) {
                override void visit(Node importedDecl) {
                    static if (isDeclaration!Node) {
                        scope_.addDeclaration(importedDecl);
                    }
                }
            }
        }

        unit.accept(new ImportVisitor);
    }

private:

    Scope scope_;

    alias RelevantNodes = Erase!(Unit, NoDuplicates!(AliasSeq!(Nodes.Scopes, Nodes.Declarations)));

    enum isScope(T) = staticIndexOf!(T, Nodes.Scopes) >= 0;
    enum isDeclaration(T) = staticIndexOf!(T, Nodes.Declarations) >= 0;
}

private class Scope {
    import abstrac.utils.stack : Stack;

    AAstNode node;
    AAstNode[][string] declarations;
    Identifier[][string] identifiers;
    Scope parent;
    Scope[] children;

    this(AAstNode node, Scope parent) {
        this.node = node;
        this.parent = parent;
        if (parent !is null) {
            parent.children ~= this;
        }
    }

    void addDeclaration(AAstNode arg) {
        if (arg.value == "") {
            return;
        }
        if (auto array = arg.value in declarations) {
            *array ~= arg;
        } else {
            declarations[arg.value] = [arg];
        }
    }

    void addIdentifier(Identifier arg) {
        assert(arg.value != "");
        if (auto array = arg.value in identifiers) {
            *array ~= arg;
        } else {
            identifiers[arg.value] = [arg];
        }
    }

    private void collectDeclarations(string name, ref AAstNode[] collection) {
        if (auto declarationArray = name in declarations) {
            collection ~= *declarationArray;
        }
        if (parent !is null) {
            parent.collectDeclarations(name, collection);
        }
    }

    void bindIdentifiers() {
        foreach (name, identifierArray; identifiers) {
            AAstNode[] collection;
            collectDeclarations(name, collection);
            AAstNode binding;
            if (collection.length == 0) {
                throw new Exception("Undefined identifier: " ~ name);
            } else if (collection.length == 1) {
                binding = collection[0];
            } else {
                import abstrac.ast.makers : makeOverloadSet;

                binding = makeOverloadSet(collection[]);
            }
            foreach (identifier; identifierArray) {
                bindIdentifier(identifier, binding);
            }
        }
        foreach (child; children) {
            child.bindIdentifiers();
        }
    }

    void bindIdentifier(Identifier arg, AAstNode binding) {
        import abstrac.ast.makers : makeBoundIdentifier;

        replace(arg, makeBoundIdentifier(binding));
    }

}
