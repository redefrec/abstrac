module abstrac.manipulation.restructure;

import abstrac.manipulation : TManipulator;
import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.ast.makers;
import std.meta : AliasSeq;
import std.string : toStringz;

class RestructureManipulator : TManipulator!false {
    enum Order = 0;

    this() {
        super(Order);
    }

    private bool singleChildReplace(AAstNode parent) {
        if (parent.children.length == 1) {
            auto replacer = parent.child(0);
            replace(parent, replacer);
            replacer.accept(this);
            return true;
        }
        return false;
    }

    static foreach (Node; AliasSeq!(FunctionDeclaration, AliasDeclaration,
            PrecomputeDeclaration, StructDeclaration)) {
        override void visit(Node arg) {
            visitChildren(arg);
            TemplateParameterList paramList = tryGetUniqueChild!TemplateParameterList(arg);
            if (paramList !is null) {
                auto placeholder = makePlaceholder();
                replace(arg, placeholder);
                replace(placeholder, makeTemplateDeclaration(paramList, arg));
            }
        }
    }

    private void collectInfixOperators(C)(InfixExpression arg, ref C array) {
        assert(arg.children.length % 2 == 1);
        for (size_t i = 1; i < arg.children.length; i += 2) {
            auto op = cast(CustomInfixOperator) arg.child(i);
            assert(op !is null);
            if (array.empty() || array[0].precedence == op.precedence) {
                array ~= op;
            } else if (array[0].precedence > op.precedence) {
                array.clear();
                array ~= op;
            }
        }
    }

    private AAstNode raiseInfixOperator(CustomInfixOperator arg) {
        InfixExpression leftExpression = new InfixExpression;
        InfixExpression rightExpression = new InfixExpression;
        auto index = indexOf(arg.parent, arg);
        assert(index.found);
        moveChildRange(arg.parent, rightExpression, index.index + 1, arg.parent.children.length);
        moveChildRange(arg.parent, leftExpression, 0, index.index);
        auto functionId = makeIdentifier(toStringz(arg.value));
        auto replacer = makePostfixCallOperator([leftExpression, rightExpression], functionId);
        replace(arg.parent, replacer);
        remove(arg);
        return replacer;
    }

    override void visit(InfixExpression arg) {
        import std.container.array : Array;
        import std.algorithm : all;

        if (!singleChildReplace(arg)) {
            Array!CustomInfixOperator ops;
            collectInfixOperators(arg, ops);
            assert(all!(a => a.leftAssociative == ops[0].leftAssociative)(ops[]));
            AAstNode replacer;
            for (size_t i = 0; i < ops.length; i++) {
                auto op = ops[ops[0].leftAssociative ? $ - (i + 1) : i];
                replacer = raiseInfixOperator(op);
                replacer.child(ops[0].leftAssociative ? 1 : 0).accept(this);
            }
            replacer.child(ops[0].leftAssociative ? 0 : 1).accept(this);
        }
    }

    private void raiseEdgeOperator(P, O)(P parent, O operator) {
        remove(operator);
        replace(parent, operator);
        append(operator, parent);
        parent.accept(this);
    }

    override void visit(PrefixExpression arg) {
        if (singleChildReplace(arg)) {
            return;
        }
        assert(arg.children.length > 1);
        visitChildren(arg);
        raiseEdgeOperator(arg, arg.child(0));
    }

    override void visit(PostfixExpression arg) {
        if (singleChildReplace(arg)) {
            return;
        }
        assert(arg.children.length > 1);
        visitChildren(arg);
        raiseEdgeOperator(arg, arg.lastChild());
    }

    override void visit(Type arg) {
        while (arg.children.length > 1) {
            auto beforeSuffix = arg.child(0);
            auto firstSuffix = arg.child(1);
            moveChild(beforeSuffix, firstSuffix);
        }
        visitChildren(arg);
    }

    override void visit(CharacterLiteral arg) {
        import std.array : appender;

        auto replacements = [
            '0' : '\0', '\\' : '\\', '\'' : '\'', '\"' : '\"', 'a' : '\a', 'b'
            : '\b', 'f' : '\f', 'n' : '\n', 'r' : '\r', 't' : '\t', 'v' : '\v'
        ];
        auto builder = appender!string();
        builder.reserve(arg.value.length);
        bool escaped = false;
        for (size_t i = 0; i < arg.value.length; i++) {
            if (escaped) {
                auto replacement = arg.value[i] in replacements;
                assert(replacement !is null);
                builder ~= *replacement;
                escaped = false;
            } else {
                if (arg.value[i] == '\\') {
                    escaped = true;
                    continue;
                }
                builder ~= arg.value[i];
            }
        }
        arg.value = builder.data();
    }

}
