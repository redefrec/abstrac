module abstrac.log;

enum LogLevel {
    Trace,
    Info,
    Warning,
    Err,
    Fatal
}

struct Logger(LogLevel threshold) {
    string name;

    void log(Args...)(LogLevel level, Args args) {
        if (level >= threshold) {
            import std.stdio : writeln;
            import std.conv : to;
            import std.string : leftJustify;

            writeln("[" ~ to!string(level) ~ " | " ~ name ~ "]: ", args);
        }
    }

    void trace(Args...)(Args args) {
        log(LogLevel.Trace, args);
    }

    void info(Args...)(Args args) {
        log(LogLevel.Info, args);
    }

    void warning(Args...)(Args args) {
        log(LogLevel.Warning, args);
    }

    void error(Args...)(Args args) {
        log(LogLevel.Err, args);
    }

    void fatal(Args...)(Args args) {
        log(LogLevel.Fatal, args);
    }
}

template functionLog(args...) {
    import std.range : join;

    static if (args.length > 0) {
        enum functionLog = `logger.trace("vvvvvvvvvv ", __FUNCTION__, ":", __LINE__, " (", ` ~ join([args],
                    ", \", \", ") ~ `, ")");
                    scope(success) logger.trace("---------- ", __FUNCTION__, ":", __LINE__, " (", ` ~ join(
                    [args], ", \", \", ") ~ `, ")");`;
    } else {
        enum functionLog = `logger.trace("vvvvvvvvvv ", __FUNCTION__, ":", __LINE__);
                            scope(success) logger.trace("---------- ", __FUNCTION__, ":", __LINE__);`;
    }
}

void printError(Args...)(Args args) {
    import std.stdio : writeln;

    writeln(args);
}
