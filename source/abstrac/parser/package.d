module abstrac.parser;

import abstrac.parser.context : ParseContext, FileStream;
import abstrac.parser.rules.grammar : makeLanguageRules, pluginRules, EntryPoint;
import abstrac.parser.rules : ARule, fmatch;
import abstrac.ast;
import abstrac.log;
import std.array : array;
import std.file;
import std.path;

class Parser {

    private string[] sourceFiles;
    private string[] sourceDirectories;
    private const ARule prelimParseRule;
    private PreliminaryUnit[string] prelimUnits;
    private Unit[string] units;

    this(string[] sourceFiles, string[] sourceDirectories) {
        this.sourceFiles = sourceFiles;
        this.sourceDirectories = sourceDirectories;
        foreach (ref directory; this.sourceDirectories) {
            directory = normalizePath(directory);
        }
        ARule* prelimLookup = "PreliminaryUnit" in makeLanguageRules();
        assert(prelimLookup !is null);
        prelimParseRule = *prelimLookup;
    }

    private static string normalizePath(string arg) {
        return array(asNormalizedPath(asAbsolutePath(arg)));
    }

    private string findSourceFile(string arg) {
        if (exists(arg) && isFile(arg)) {
            return normalizePath(arg);
        }
        foreach (directory; sourceDirectories) {
            string inDirectory = chainPath(directory, arg).array;
            if (exists(inDirectory) && isFile(inDirectory)) {
                return normalizePath(inDirectory);
            }
        }
        printError("Couldn't find source file '", arg, "' in ", [getcwd()] ~ sourceDirectories);
        assert(0);
    }

    private string toUnitIdentifier(string arg) {
        import std.algorithm : startsWith;
        import std.range : split, join;

        foreach (directory; sourceDirectories) {
            if (arg.startsWith(directory)) {
                string asRelative = asRelativePath(arg, directory).array;
                asRelative = stripExtension(asRelative);
                asRelative = asRelative.split(dirSeparator).join('.');
                return asRelative;
            }
        }
        printError("Source file '", arg, "' not in a source directory: ", sourceDirectories);
        assert(0);
    }

    private string unitIdToAbsolutePath(string unitId) {
        import std.range : split, join;

        string relativePath = unitId.split(".").join(dirSeparator) ~ ".ac";
        string absolutePath = findSourceFile(relativePath);
        return absolutePath;
    }

    private PreliminaryUnit prelimParse(string filepath) {
        ParseContext context = new ParseContext(new FileStream(filepath));
        fmatch(context, prelimParseRule);
        return context.getSingleResult!PreliminaryUnit();
    }

    private void connectImports(PreliminaryUnit prelim) {
        import abstrac.ast.helpers : pathSelect;
        import std.algorithm : canFind;

        auto importNodes = pathSelect!UnitImport(prelim);
        foreach (importNode; importNodes) {
            bool isPublic = pathSelect!PublicVisibility(importNode).length == 1;
            string unitId = importNode.value;
            PreliminaryUnit* present = unitId in prelimUnits;
            PreliminaryUnit importedPrelim;
            if (present is null) {
                importedPrelim = prelimParse(unitIdToAbsolutePath(unitId));
                prelimUnits[unitId] = importedPrelim;
            } else {
                importedPrelim = *present;
            }
            connectImports(importedPrelim);
            if (!prelim.imports.canFind!(a => a.prelim is importedPrelim)) {
                prelim.imports ~= Import(importedPrelim, isPublic);
            }
        }
    }

    private static void addExtensionRules(ARule[string] rules, Extension extension) {
        import abstrac.ast.helpers;

        auto grammarProp = tryGetUniqueChild!ExtensionGrammarProperty(extension);
        auto hooksProp = tryGetUniqueChild!ExtensionHooksProperty(extension);

        assert((grammarProp !is null) == (hooksProp !is null));
        if (grammarProp !is null) {
            auto grammar = getUniqueChild!Grammar(grammarProp);
            auto hooks = pathSelect!ExtensionHook(hooksProp);
            EntryPoint[] entryPoints = [];
            foreach (hook; hooks) {
                auto nodeName = hook.child(0).value;
                auto targetName = hook.child(1).value;
                auto parseOrder = parseLongValue(hook.child(2));
                entryPoints ~= EntryPoint(nodeName, targetName, parseOrder);
            }
            pluginRules(rules, grammar, entryPoints);
        }
    }

    private static void addExtensionRules(ARule[string] rules, PreliminaryUnit unit) {
        import abstrac.ast.helpers : pathSelect;

        Extension[] extensions = pathSelect!Extension(unit);
        foreach (extension; extensions) {
            addExtensionRules(rules, extension);
        }
    }

    private ARule[string] makeUnitRules(PreliminaryUnit arg) {
        auto rules = makeLanguageRules();
        addExtensionRules(rules, arg);
        foreach (import_; arg.imports) {
            addExtensionRules(rules, import_.prelim);
        }
        return rules;
    }

    Unit[string] parseFiles() {
        foreach (sourceFile; sourceFiles) {
            auto absolutePath = findSourceFile(sourceFile);
            auto unitId = toUnitIdentifier(absolutePath);
            if (auto lookup = unitId in prelimUnits) {
                auto prelim = *lookup;
                prelim.emitted = true;
            } else {
                auto prelim = prelimParse(absolutePath);
                prelim.emitted = true;
                prelimUnits[unitId] = prelim;
                connectImports(prelim);
            }
        }
        foreach (unitId, preliminaryUnit; prelimUnits) {
            auto rules = makeUnitRules(preliminaryUnit);
            auto unitRule = rules["Unit"];
            auto absolutePath = unitIdToAbsolutePath(unitId);
            FileStream stream = new FileStream(absolutePath);
            ParseContext context = new ParseContext(stream);
            fmatch(context, unitRule);
            auto unit = context.getSingleResult!Unit();
            unit.value = unitId;
            unit.imports = preliminaryUnit.imports;
            unit.emitted = preliminaryUnit.emitted;
            units[unitId] = unit;
        }
        return units;
    }

}
