module abstrac.parser.context.stream;

class ParseStreamState {
    abstract void restore();
}

class ParseStream {
    abstract {
        char peek() const;
        char read();
        string read(size_t num);
        string slice(size_t begin, size_t end) const;
        string debugPeekString(size_t size = 0) const;
        size_t bytesLeft() const;
        bool eof() const;
        bool bof() const;
        ParseStreamState tell();
        string name() const;
        string locationString() const;
    }
}
