module abstrac.parser.context.stack;

class ParseStack {
    import abstrac.ast : AAstNode;
    import std.container.array : Array;

    Array!AAstNode nodes;
    Array!char chars;

    T popNode(T)() {
        assert(!nodes.empty());
        auto node = nodes.back();
        auto asT = cast(T) node;
        assert(asT !is null, "Cannot pop node " ~ typeid(node).name ~ " as " ~ T.stringof ~ "!");
        nodes.removeBack();
        return asT;
    }

    void pushNode(AAstNode node) {
        nodes ~= node;
    }

    void pushString(string value) {
        foreach (c; value) {
            pushChar(c);
        }
    }

    void pushChar(char c) {
        chars ~= c;
    }

    private static struct StackState {
        private ParseStack stack;
        private size_t chars;
        private size_t nodes;

        void restore() {
            assert(stack.nodes.length >= nodes);
            assert(stack.chars.length >= chars);
            stack.nodes.length = nodes;
            stack.chars.length = chars;
        }

        string stringSlice() {
            import std.range : join;
            import std.conv : to;

            return to!string(join([stack.chars[chars .. $]]));
        }

        auto nodesSlice() {
            return stack.nodes[nodes .. $];
        }
    }

    StackState tell() {
        return StackState(this, chars.length(), nodes.length());
    }

}
