module abstrac.parser.context.file_stream;

import abstrac.parser.context.stream;

private class FileStreamState : ParseStreamState {
    private FileStream stream;
    private size_t position;
    private size_t line;
    private size_t column;
    private bool consumed = false;

    this(FileStream stream) {
        this.stream = stream;
        this.position = stream.position;
        this.line = stream.line;
        this.column = stream.column;
    }

    override void restore() {
        stream.position = position;
        stream.line = line;
        stream.column = column;
    }
}

class FileStream : ParseStream {
    import std.stdio : File;

    private string _name;
    private string content;
    private size_t position;
    private size_t line = 1;
    private size_t column = 1;

    this(string filepath) {
        import std.file : readText;

        this._name = filepath;
        this.content = readText(filepath);
    }

    override char peek() const {
        return position >= content.length ? '\0' : content[position];
    }

    override char read() {
        const c = peek();
        ++column;
        if (c == '\n') {
            ++line;
            column = 1;
        }
        ++position;
        return c;
    }

    override string read(size_t num) {
        assert(num <= bytesLeft());
        string result = content[position .. position + num];
        foreach (c; result) {
            ++column;
            if (c == '\n') {
                ++line;
                column = 1;
            }
        }
        position += num;
        return result;
    }

    override string slice(size_t begin, size_t end) const {
        assert(end >= begin);
        assert(end <= content.length);
        return content[begin .. end];
    }

    override string debugPeekString(size_t size = 0) const {
        import std.algorithm : min;
        import std.array : replace;

        if (position >= content.length) {
            return "";
        }
        string value;
        if (size == 0) {
            value = content[position .. $];
        } else {
            value = content[position .. min(position + size, content.length)];
        }
        value = value.replace("\n", "\\n").replace("\r", "\\r");
        return value;
    }

    override size_t bytesLeft() const {
        return content.length - position;
    }

    override bool eof() const {
        return position >= content.length;
    }

    override bool bof() const {
        return position == 0;
    }

    override ParseStreamState tell() {
        return new FileStreamState(this);
    }

    override string name() const {
        return _name;
    }

    override string locationString() const {
        import std.conv : to;

        return name() ~ ":" ~ to!string(line) ~ ":" ~ to!string(column);
    }
}
