module abstrac.parser.context;

public import abstrac.parser.context.stream;
public import abstrac.parser.context.stack;
public import abstrac.parser.context.string_stream;
public import abstrac.parser.context.file_stream;

class ParseContext {
    import std.container.array : Array;

    ParseStream stream;
    ParseStack stack;
    private Array!string diagnosticHints;

    this(ParseStream stream) {
        this.stream = stream;
        this.stack = new ParseStack;
    }

    private static struct ContextState {
        private ParseContext context;
        private typeof(stream.tell()) streamState;
        private typeof(stack.tell()) stackState;

        void restore() {
            streamState.restore();
            stackState.restore();
        }
    }

    ContextState tell() {
        return ContextState(this, stream.tell(), stack.tell());
    }

    auto tellDiagnosticState() {
        return diagnosticHints.length();
    }

    void seekDiagnosticState(size_t state) {
        diagnosticHints.length = state;
    }

    void pushDiagnosticHint(string text) {
        const location = stream.locationString() ~ "\n";
        text = location ~ text;
        diagnosticHints ~= text;
    }

    auto getDiagnosticHints() {
        return diagnosticHints[];
    }

    T getSingleResult(T)() {
        assert(stack.nodes.length <= 1);
        if (stack.nodes.length == 0) {
            foreach (diagnosticHint; getDiagnosticHints()) {
                import std.stdio : writeln;

                writeln(diagnosticHint);
            }
            assert(0);
        }
        auto asResultType = cast(T) stack.nodes[0];
        assert(asResultType !is null);
        return asResultType;
    }
}
