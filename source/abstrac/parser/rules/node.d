module abstrac.parser.rules.node;

import abstrac.parser.rules;
import abstrac.ast : CustomAstNode;

static class NodePlaceholder : ARule {

    string name;

    override bool match(ParseContext context) const {
        assert(0, "Unmatchable placeholder rule!");
    }

    override string toString() const {
        return "";
    }

    override string shallowToString(size_t level = 1) const {
        return "";
    }

    override void replacePlaceholder(ARule delegate(string) producer) {
        assert(0);
    }
}

static ARule node(CaptureT, ListRuleType type, bool singleChildFold = false, Args...)(
        string nodeName, Args args) {
    static class NodeRule : TListRule!type {

        string nodeName;

        this(string nodeName, Args args) {
            this.nodeName = nodeName;
            super(args);
        }

        override bool match(ParseContext context) const {
            auto stackState = context.stack.tell();
            const success = super.match(context);
            static if (!is(CaptureT == void)) {
                if (success) {
                    import std.array : join;
                    import std.conv : to;

                    auto nodes = stackState.nodesSlice;
                    static if (singleChildFold) {
                        if (nodes.length == 1) {
                            return success;
                        }
                    }
                    auto chars = stackState.stringSlice;
                    CaptureT capture = new CaptureT;
                    static if (is(CaptureT == CustomAstNode)) {
                        capture.name = nodeName;
                    }
                    foreach (node; nodes) {
                        import abstrac.ast.helpers : append;

                        append(capture, node);
                    }
                    capture.value = to!string(join([chars]));
                    stackState.restore();
                    context.stack.pushNode(capture);
                }
            }
            return success;
        }

        override string toString() const {
            return nodeName ~ super.toString();
        }

        override string shallowToString(size_t level = 1) const {
            return nodeName ~ super.shallowToString(level);
        }

        override bool isNode() const {
            return true;
        }
    }

    auto result = new NodeRule(nodeName, args);
    return result;
}

static ARule rtNode(CaptureT, Args...)(ListRuleType type, bool singleChildFold,
        string nodeName, Args args) {
    final switch (type) {
    case ListRuleType.Sequence:
        if (singleChildFold) {
            return node!(CaptureT, ListRuleType.Sequence, true)(nodeName, args);
        } else {
            return node!(CaptureT, ListRuleType.Sequence, false)(nodeName, args);
        }
    case ListRuleType.Choice:
        if (singleChildFold) {
            return node!(CaptureT, ListRuleType.Choice, true)(nodeName, args);
        } else {
            return node!(CaptureT, ListRuleType.Choice, false)(nodeName, args);
        }
    }
}
