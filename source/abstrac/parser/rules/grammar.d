module abstrac.parser.rules.grammar;

import abstrac.ast.grammar;
import abstrac.ast : CustomAstNode;
import abstrac.parser.rules;
import abstrac.log;
import std.meta : Alias, staticMap;

auto logger = Logger!(LogLevel.Info)(__FILE__);

private static ARule grammarFileRule;
private static string grammarFolderContents;
private static GrammarFile defaultGrammar;

static this() {
        createGrammarParserRule();
        defaultGrammar = parseGrammarFolder();
}

static ARule[string] makeLanguageRules() {
        import abstrac.parser.rules.from_grammar : grammarNodeToRuleset;

        return grammarNodeToRuleset(cast(Grammar) defaultGrammar.child(0));
}

struct EntryPoint {
        string newNode;
        string oldNode;
        long rank;
}

static void pluginRules(ARule[string] rules, Grammar plugin, EntryPoint[] entryPoints) {
        import abstrac.parser.rules.from_grammar : grammarNodeToRuleset;

        auto newRules = grammarNodeToRuleset(plugin, rules);
        foreach (entryPoint; entryPoints) {
                ARule oldRule = rules[entryPoint.oldNode];
                ARule newRule = newRules[entryPoint.newNode];
                oldRule.addElement(ListRuleElement(newRule, entryPoint.rank));
        }
        foreach (name, rule; newRules) {
                assert(name !in rules);
                rules[name] = rule;
        }
}

private static void createGrammarParserRule() {
        enum And = ListRuleType.Sequence;
        enum Or = ListRuleType.Choice;
        alias toRule(alias value) = ARule;
        staticMap!(toRule, predefinedRuleStrings) predefCapstrings;
        static foreach (i, predefinedRuleString; predefinedRuleStrings) {
                predefCapstrings[i] = capstr(predefinedRuleString);
        }
        ARule predef = node!(GrammarPredefinedRule, Or)("GrammarPredefinedRule", predefCapstrings);
        ARule name = node!(void, And)("GrammarName", upper, star(alnum));
        ARule reference = node!(GrammarNodeId, And)("GrammarNodeId", upper, star(alnum));
        ARule escapedChar = node!(void, Or)("GrammarEscapedCharacter",
                        capstr("\\"), capstr("\""), capstr("'"));
        ARule escapeSeq = node!(void, And)("GrammarEscapeSequence", noncapstr("\\"), escapedChar);
        ARule literalChar = node!(void, And)("GrammarLiteralCharacter", notAt(escapedChar), any);
        ARule quotedChar = node!(void, Or)("GrammarQuotedCharacter", literalChar, escapeSeq);
        ARule sqStr = node!(GrammarSqStr, And)("GrammarSqStr", noncapstr("'"),
                        plus(quotedChar), noncapstr("'"));
        ARule dqStr = node!(GrammarDqStr, And)("GrammarDqStr", noncapstr("\""),
                        plus(quotedChar), noncapstr("\""));
        ARule keyword = node!(GrammarKeyword, And)("GrammarKeyword",
                        noncapstr("<"), diag(cho(sqStr, dqStr)), diag(noncapstr(">")));
        ARule atom = node!(void, Or)("GrammarAtom", sqStr, dqStr, keyword, predef, reference);
        ARule spacing = node!(GrammarSpacing, Or)("GrammarSpacing", capstr("$"), capstr("%"));
        ARule modifier = node!(GrammarModifier, Or)("GrammarModifier", capstr("?"),
                        capstr("*"), capstr("+"), capstr("@"), capstr("!"), capstr("&"));
        ARule combo = node!(void, And)("GrammarCombo", star(modifier), opt(spacing), diag(atom));
        ARule rankElem = node!(GrammarTopListElement, And)("GrammarTopListElement",
                        combo, sep, diag(noncapstr(":")), sep, diag(plus(digit)));
        ARule rankElems = node!(void, And)("GrammarTopListElements", rankElem,
                        star(seq(sep, noncapstr(","), sep, rankElem)));
        ARule topAnd = node!(GrammarTopSequence, And)("GrammarTopSequence",
                        noncapstr("("), sep, rankElems, sep, diag(noncapstr(")")));
        ARule topOr = node!(GrammarTopChoice, And)("GrammarTopChoice",
                        noncapstr("["), sep, rankElems, sep, diag(noncapstr("]")));
        ARule folding = node!(GrammarFolding, Or)("GrammarFolding", capstr("#"), capstr("/"));
        ARule topNode = node!(GrammarTopNode, And)("GrammarTopNode", opt(folding), name);
        ARule topRule = node!(GrammarTopRule, And)("GrammarTopRule", topNode,
                        diag(noncapstr(":")), sep, diag(cho(topAnd, topOr)));
        ARule grammarRule = node!(Grammar, And)("Grammar", star(seq(sep, topRule)));
        grammarFileRule = node!(GrammarFile, And)("GrammarFile", bof, grammarRule, sep, diag(eof));
}

private static GrammarFile parseGrammarFolder() {
        grammarFolderContents = readGrammarFolder();
        auto stream = new StringStream(grammarFolderContents);
        ParseContext context = new ParseContext(stream);
        fmatch(context, grammarFileRule);
        return context.getSingleResult!GrammarFile();
}

private static string readGrammarFolder(string folder = "./grammar") {
        import std.file : readText, dirEntries, SpanMode;
        import std.algorithm : filter;
        import std.string : endsWith;

        string contents = "";
        foreach (string filepath; dirEntries(folder, SpanMode.depth).filter!(
                        a => a.name.endsWith(".acg"))) {
                contents ~= readText(filepath) ~ "\n\n";
        }
        return contents;
}
