module abstrac.parser.rules.from_grammar;

import abstrac.parser.rules;
import abstrac.ast.grammar;
import abstrac.utils.visitor : TVisitor;

ARule[string] grammarNodeToRuleset(Grammar arg, ARule[string] existingRules = null) {
    Visitor visitor = new Visitor;
    visitor.existingRules = existingRules;
    arg.accept(visitor);
    return visitor.rules;
}

private class Visitor : TVisitor!true {
    import std.container.array : Array;

    ARule[string] rules;
    ARule[string] existingRules;

    private {
        SpacingType currentElementSpacingType = SpacingType.Ignore;
        Array!string currentElementModifiers;
        ARule currentElementRule;

        string currentNodeName;
        FoldingType currentNodeFoldingType;
        ListRuleType currentNodeListType;
        ListRuleElement[] currentNodeSubrules;

        enum FoldingType {
            Always,
            SingleChild,
            Never
        }

        enum SpacingType {
            Require,
            Ignore,
            Prevent
        }

        void resolveNodeRules() {
            foreach (_, rule; rules) {
                rule.replacePlaceholder((string name) {
                    ARule* referencedRule = name in rules;
                    if (referencedRule is null) {
                        referencedRule = name in existingRules;
                    }
                    assert(referencedRule !is null, "Couldn't reference grammar node: " ~ name);
                    return *referencedRule;
                });
            }
        }

        ARule assembleAtom(ARule rule) {
            if (currentNodeListType == ListRuleType.Sequence) {
                final switch (currentElementSpacingType) {
                case SpacingType.Ignore:
                    rule = seq(sep, rule);
                    break;
                case SpacingType.Prevent:
                    break;
                case SpacingType.Require:
                    rule = seq(space, rule);
                    break;
                }
            }
            while (!currentElementModifiers.empty()) {
                const modifier = currentElementModifiers.back();
                currentElementModifiers.removeBack();
                switch (modifier) {
                case "?":
                    rule = opt(rule);
                    break;
                case "*":
                    rule = star(rule);
                    break;
                case "+":
                    rule = plus(rule);
                    break;
                case "@":
                    rule = at(rule);
                    break;
                case "!":
                    rule = notAt(rule);
                    break;
                case "<":
                    rule = seq(rule, notAt(cho(noncapstr("_"), alnum)));
                    break;
                case "&":
                    rule = diag(rule);
                    break;
                default:
                    assert(0, "Unrecognized modifier: " ~ modifier);
                }
            }
            return rule;
        }

        void createNode() {
            import std.meta : Erase;
            import abstrac.ast : Nodes, CustomAstNode;

            if (currentNodeFoldingType == FoldingType.Always) {
                rules[currentNodeName] = rtNode!void(currentNodeListType,
                        false, currentNodeName, currentNodeSubrules);
            } else {
            outer:
                switch (currentNodeName) {
                    static foreach (Node; Erase!(CustomAstNode, Nodes.All)) {
                case __traits(identifier, Node):
                        rules[currentNodeName] = rtNode!Node(currentNodeListType,
                                currentNodeFoldingType == FoldingType.SingleChild,
                                currentNodeName, currentNodeSubrules);
                        break outer;
                    }
                default:
                    rules[currentNodeName] = rtNode!CustomAstNode(currentNodeListType,
                            currentNodeFoldingType == FoldingType.SingleChild,
                            currentNodeName, currentNodeSubrules);
                    if (existingRules is null) {
                        assert(0, currentNodeName);
                    }
                }
            }
            currentNodeName = "invalid";
            currentNodeSubrules = [];
        }
    }

    override {
        void visit(Grammar arg) {
            visitChildren(arg);
            resolveNodeRules();
        }

        void visit(GrammarTopRule arg) {
            visitChildren(arg);
        }

        void visit(GrammarTopNode arg) {
            currentNodeName = arg.value;
            import abstrac.ast.helpers : pathSelect;

            const foldings = pathSelect!GrammarFolding(arg);
            if (foldings.length == 0) {
                currentNodeFoldingType = FoldingType.Never;
                return;
            }
            const folding = foldings[0];
            if (folding.value == "#") {
                currentNodeFoldingType = FoldingType.Always;
            } else if (folding.value == "/") {
                currentNodeFoldingType = FoldingType.SingleChild;
            }
        }

        void visit(GrammarTopSequence arg) {
            currentNodeListType = ListRuleType.Sequence;
            visitChildren(arg);
            createNode();
        }

        void visit(GrammarTopChoice arg) {
            currentNodeListType = ListRuleType.Choice;
            visitChildren(arg);
            createNode();
        }

        void visit(GrammarSpacing arg) {
            if (arg.value == "$") {
                currentElementSpacingType = SpacingType.Require;
            } else if (arg.value == "%") {
                currentElementSpacingType = SpacingType.Prevent;
            } else {
                assert(0);
            }
        }

        void visit(GrammarModifier arg) {
            currentElementModifiers ~= arg.value;
        }

        void visit(GrammarKeyword arg) {
            currentElementModifiers ~= "<";
            visitChildren(arg);
        }

        void visit(GrammarSqStr arg) {
            currentElementRule = assembleAtom(noncapstr(arg.value));
        }

        void visit(GrammarDqStr arg) {
            currentElementRule = assembleAtom(capstr(arg.value));
        }

        void visit(GrammarPredefinedRule arg) {
        outer:
            final switch (arg.value) {
                static foreach (predefRuleString; predefinedRuleStrings) {
            case predefRuleString:
                    mixin("currentElementRule = assembleAtom(" ~ predefRuleString ~ ");");
                    break outer;
                }
            }
        }

        void visit(GrammarNodeId arg) {
            NodePlaceholder placeholder = new NodePlaceholder;
            placeholder.name = arg.value;
            currentElementRule = assembleAtom(placeholder);
        }

        void visit(GrammarTopListElement arg) {
            import std.conv : to;

            currentElementSpacingType = SpacingType.Ignore;
            visitChildren(arg);
            const rank = to!long(arg.value);
            currentNodeSubrules ~= ListRuleElement(currentElementRule, rank);
        }
    }
}
