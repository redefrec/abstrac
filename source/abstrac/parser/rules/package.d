module abstrac.parser.rules;

public import abstrac.parser.rules.atomic;
public import abstrac.parser.rules.composite;
public import abstrac.parser.rules.node;
public import abstrac.parser.context;

import abstrac.log;
import std.meta : AliasSeq;

auto logger = Logger!(LogLevel.Info)(__FILE__);

bool fmatch(ParseContext context, const ARule rule) {
    import std.range : repeat;

    static size_t indent = 0;
    string streamPeek = context.stream.debugPeekString(10);
    indent += 2;
    const success = rule.match(context);
    indent -= 2;
    if (rule.isNode()) {
        logger.trace(' '.repeat(indent), (success ? "SUCCESS: '" : "FAILURE: '"),
                streamPeek, "' with rule: ", rule.toString());
    }
    return success;
}

abstract class ARule {
    abstract bool match(ParseContext context) const;
    abstract string shallowToString(size_t level = 1) const;
    override abstract string toString() const;

    void replacePlaceholder(ARule delegate(string)) {
    }

    bool isNode() const {
        return false;
    }

    void addElement(ListRuleElement) {
        assert(0);
    }
}

alias predefinedRuleStrings = AliasSeq!("alnum", "alpha", "lower", "upper", "hex",
        "octal", "digit", "binary", "white", "any", "sep", "space", "bof", "eof");
