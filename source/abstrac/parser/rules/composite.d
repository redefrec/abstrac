module abstrac.parser.rules.composite;

import abstrac.parser.rules;

static ARule noncapstr(string value) {
    return new StringRule!(false)(value);
}

static ARule capstr(string value) {
    return new StringRule!(true)(value);
}

static ARule at(ARule subrule) {
    return new LookAheadRule!false(subrule);
}

static ARule notAt(ARule subrule) {
    return new LookAheadRule!true(subrule);
}

static ARule diag(ARule subrule) {
    return new DiagRule(subrule);
}

static ARule opt(ARule subrule) {
    return new NumberedRule!(0, 1)(subrule);
}

static ARule star(ARule subrule) {
    return new NumberedRule!(0)(subrule);
}

static ARule plus(ARule subrule) {
    return new NumberedRule!(1)(subrule);
}

static ARule seq(Args...)(Args args) {
    return new TListRule!(ListRuleType.Sequence)(args);
}

static ARule cho(Args...)(Args args) {
    return new TListRule!(ListRuleType.Choice)(args);
}

private static class StringRule(bool capture) : ARule {
    immutable string value;

    this(string value) {
        this.value = value;
    }

    override bool match(ParseContext context) const {
        if (context.stream.bytesLeft() < value.length) {
            return false;
        }
        auto begin = context.stream.tell();
        string read = context.stream.read(value.length);
        if (read != value) {
            begin.restore();
            return false;
        }
        static if (capture) {
            context.stack.pushString(value);
        }
        return true;
    }

    override string toString() const {
        if (capture) {
            return "\"" ~ value ~ "\"";
        } else {
            return "'" ~ value ~ "'";
        }
    }

    override string shallowToString(size_t level = 1) const {
        return toString();
    }
}

private static class LookAheadRule(bool negate) : ARule {
    mixin Subrule;

    override bool match(ParseContext context) const {
        auto saved = context.tell();
        const success = fmatch(context, subrule);
        saved.restore();
        return negate != success;
    }

    override string toString() const {
        return (negate ? "notAt" : "at") ~ "(" ~ subrule.toString() ~ ")";
    }

    override string shallowToString(size_t level = 1) const {
        return (negate ? "notAt" : "at") ~ "(" ~ subrule.shallowToString(level) ~ ")";
    }
}

private static class DiagRule : ARule {
    mixin Subrule;

    override bool match(ParseContext context) const {
        const success = fmatch(context, subrule);
        if (!success) {
            string expected = "Expected " ~ subrule.toString() ~ ";\n";
            string got = "Instead got: '" ~ context.stream.debugPeekString(20) ~ "'\n";
            context.pushDiagnosticHint(expected ~ got);
        }
        return success;
    }

    override string toString() const {
        return subrule.toString();
    }

    override string shallowToString(size_t level = 1) const {
        return subrule.shallowToString(level);
    }
}

private static class NumberedRule(long atLeast, long atMost = -1) : ARule {
    static assert(atLeast >= 0);
    static assert(atLeast <= atMost || atMost == -1);

    mixin Subrule;

    override bool match(ParseContext context) const {
        auto saved = context.tell();
        static foreach (i; 0 .. atLeast) {
            if (!fmatch(context, subrule)) {
                saved.restore();
                return false;
            }
        }
        static if (atMost == -1) {
            while (fmatch(context, subrule)) {
            }
        } else {
            static foreach (i; atLeast .. atMost) {
                if (!fmatch(context, subrule)) {
                    return true;
                }
            }
        }
        return true;
    }

    override string toString() const {
        static if (atLeast == 0 && atMost == -1) {
            return "*" ~ subrule.toString();
        } else static if (atLeast == 0 && atMost == 1) {
            return "?" ~ subrule.toString();
        } else static if (atLeast == 1 && atMost == -1) {
            return "+" ~ subrule.toString();
        } else {
            import std.conv : to;

            return "{" ~ to!string(atLeast) ~ ", " ~ to!string(atMost) ~ "}" ~ subrule.toString();
        }
    }

    override string shallowToString(size_t level = 1) const {
        static if (atLeast == 0 && atMost == -1) {
            return "*" ~ subrule.shallowToString(level);
        } else static if (atLeast == 0 && atMost == 1) {
            return "?" ~ subrule.shallowToString(level);
        } else static if (atLeast == 1 && atMost == -1) {
            return "+" ~ subrule.shallowToString(level);
        } else {
            import std.conv : to;

            return "{" ~ to!string(atLeast) ~ ", " ~ to!string(
                    atMost) ~ "}" ~ subrule.shallowToString(level);
        }
    }
}

struct ListRuleElement {
    ARule rule;
    long rank;
}

enum ListRuleType {
    Sequence,
    Choice
}

static class TListRule(ListRuleType type) : ARule {
    import std.container.array : Array;

    Array!ListRuleElement elements;

    final override void addElement(ListRuleElement element) {
        for (size_t i = 0; i < elements.length; i++) {
            if (elements[i].rank > element.rank) {
                logger.trace("Adding at: ", i);
                elements.insertBefore(elements[i .. $], element);
                return;
            }
        }
        elements.insertBack(element);
    }

    this(Args...)(Args args) {
        static if (Args.length == 1 && is(Args[0] == ListRuleElement[])) {
            foreach (rule; args[0]) {
                addElement(rule);
            }
        } else {
            static foreach (arg; args) {
                static if (is(typeof(arg) == ListRuleElement)) {
                    addElement(arg);
                } else {
                    static assert(is(typeof(arg) : ARule));
                    addElement(ListRuleElement(arg, 0));
                }
            }
        }
    }

    override bool match(ParseContext context) const {
        const diagnosticState = context.tellDiagnosticState();
        static if (type == ListRuleType.Sequence) {
            auto saved = context.tell();
            foreach (element; elements) {
                if (!fmatch(context, element.rule)) {
                    saved.restore();
                    return false;
                }
            }
            context.seekDiagnosticState(diagnosticState);
            return true;
        } else static if (type == ListRuleType.Choice) {
            foreach (element; elements) {
                if (fmatch(context, element.rule)) {
                    context.seekDiagnosticState(diagnosticState);
                    return true;
                }
            }
            return false;
        } else {
            static assert(0, "Unrecognized ListRuleType!");
        }
    }

    override string toString() const {
        import std.range : join;
        import std.algorithm : map;

        auto elementsStr = join(map!(a => a.rule.shallowToString())(elements[]), ", ");
        static if (type == ListRuleType.Sequence) {
            return "(" ~ elementsStr ~ ")";
        } else {
            return "[" ~ elementsStr ~ "]";
        }
    }

    override string shallowToString(size_t level = 1) const {
        if (level == 0) {
            static if (type == ListRuleType.Sequence) {
                return "(...)";
            } else {
                return "[...]";
            }
        } else {
            import std.range : join;
            import std.algorithm : map;

            auto elementsStr = join(map!(a => a.rule.shallowToString(level - 1))(elements[]), ", ");
            static if (type == ListRuleType.Sequence) {
                return "(" ~ elementsStr ~ ")";
            } else {
                return "[" ~ elementsStr ~ "]";
            }
        }
    }

    override void replacePlaceholder(ARule delegate(string) producer) {
        foreach (ref element; elements) {
            if (auto asPlaceholder = cast(NodePlaceholder) element.rule) {
                element.rule = producer(asPlaceholder.name);
            } else {
                element.rule.replacePlaceholder(producer);
            }
        }
    }
}

private mixin template Subrule() {
    private ARule subrule;

    this(ARule subrule) {
        this.subrule = subrule;
    }

    override void replacePlaceholder(ARule delegate(string) producer) {
        if (auto asPlaceholder = cast(NodePlaceholder) subrule) {
            subrule = producer(asPlaceholder.name);
        } else {
            subrule.replacePlaceholder(producer);
        }
    }
}
