module abstrac.parser.rules.atomic;

import abstrac.parser.rules;
import abstrac.log;
import std.ascii;

static ARule alnum;
static ARule alpha;
static ARule lower;
static ARule upper;
static ARule hex;
static ARule octal;
static ARule digit;
static ARule white;
static ARule binary;
static ARule any;
static ARule bof;
static ARule eof;
static ARule sep;
static ARule space;

static this() {
    alnum = AtomicRule!isAlphaNum.instance;
    alpha = AtomicRule!isAlpha.instance;
    lower = AtomicRule!isLower.instance;
    upper = AtomicRule!isUpper.instance;
    hex = AtomicRule!isHexDigit.instance;
    octal = AtomicRule!isOctalDigit.instance;
    digit = AtomicRule!isDigit.instance;
    binary = AtomicRule!(a => a == '0' || a == '1').instance;
    white = AtomicRule!(isWhite, false).instance;
    any = AtomicRule!(a => true).instance;
    bof = StreamPropertyRule!(a => a.bof()).instance;
    eof = StreamPropertyRule!(a => a.eof()).instance;
    sep = WhitespaceRule!false.instance;
    space = WhitespaceRule!true.instance;
}

private static class AtomicRule(alias checker, bool capture = true) : ARule {
    mixin Singleton!(typeof(this));
    override bool match(ParseContext context) const {
        if (!checker(context.stream.peek())) {
            return false;
        }
        static if (capture) {
            context.stack.pushChar(context.stream.read());
        } else {
            context.stream.read();
        }
        return true;
    }

    override string toString() const {
        return __traits(identifier, checker);
    }

    override string shallowToString(size_t level = 1) const {
        return toString();
    }
}

private static class WhitespaceRule(bool atLeastOne = false) : ARule {
    mixin Singleton!(typeof(this));
    override bool match(ParseContext context) const {
        if (!isWhite(context.stream.peek()) && atLeastOne) {
            return false;
        }
        while (isWhite(context.stream.peek())) {
            context.stream.read();
        }
        return true;
    }

    override string toString() const {
        return atLeastOne ? "space" : "sep";
    }

    override string shallowToString(size_t level = 1) const {
        return toString();
    }
}

private static class StreamPropertyRule(alias checker) : ARule {
    mixin Singleton!(typeof(this));
    override bool match(ParseContext context) const {
        return checker(&context.stream);
    }

    override string toString() const {
        return "streamProperty";
    }

    override string shallowToString(size_t level = 1) const {
        return toString();
    }
}

private mixin template Singleton(T) {
    private static ARule _instance;

    private this() {
    }

    @property static ARule instance() {
        if (_instance is null) {
            _instance = new T;
        }
        return _instance;
    }
}
