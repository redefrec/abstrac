module abstrac.codegen.conversion;

import abstrac.llvm.modules : ModuleBuilder, IModuleCache;
import abstrac.processed_values : ProcessedType, ProcessedImplicitToMDKType,
    ProcessedImplicitFromMDKType, ProcessedValue, ProcessedFunctionValue;
import abstrac.ast : FunctionDeclaration;
import abstrac.utils.multi_dispatch;
import std.container;
import std.algorithm : filter, minElement;

struct Conversion {
    ModuleBuilder mBuilder;

    this(ModuleBuilder mBuilder) {
        this.mBuilder = mBuilder;
    }

    private @property Cache cache() {
        return mBuilder.getOrCreateCache!Cache();
    }

    private static class Cache : IModuleCache {
    }

    private alias ResultT = FunctionDeclaration[];

    private static struct Step {
        ProcessedType type;
        size_t distance;
        ProcessedType predecessor;
        FunctionDeclaration function_;
        bool explored;
    }

    private static struct SearchState {
        Step[ProcessedType] forwardSteps;
        Step[ProcessedType] backwardSteps;
    }

    ProcessedType common(ProcessedType first, ProcessedType second) {
        if (find(first, second) !is null) {
            return second;
        } else if (find(second, first) !is null) {
            return first;
        } else {
            throw new Exception("Types do not share a common type: " ~ first.toString() ~ "\n" ~ second.toString());
        }
    }

    ResultT find(ProcessedType from, ProcessedType to) {
        SearchState state;
        state.forwardSteps[from] = Step(from, 0, null, null, false);
        state.backwardSteps[to] = Step(to, 0, null, null, false);
        ResultT result;

        auto ctSelect(bool cond, A, B)(A a, B b) {
            static if (cond) {
                return a;
            } else {
                return b;
            }
        }

        bool explore(bool forward)(Step step) {
            auto steps = forward ? state.forwardSteps : state.backwardSteps;
            auto otherSteps = forward ? state.backwardSteps : state.forwardSteps;
            foreach (edge; forward ? getOutgoingEdges(step.type) : getIncomingEdges(step.type)) {
                if (auto edgeStep = edge.target in steps) {
                    if (edgeStep.distance > step.distance + 1) {
                        edgeStep.distance = step.distance + 1;
                        edgeStep.predecessor = step.type;
                        edgeStep.function_ = edge.function_;
                    }
                } else {
                    steps[edge.target] = Step(edge.target, step.distance + 1,
                            step.type, edge.function_, false);
                }
                if (auto connector = edge.target in otherSteps) {
                    result = buildPath(state, edge.target);
                    return true;
                }
            }
            step.explored = true;
            steps[step.type] = step;
            return false;
        }

        while (true) {
            Step nextForwardStep;
            Step nextBackwardStep;
            bool forwardExists = getNextStep(state.forwardSteps, nextForwardStep);
            bool backwardExists = getNextStep(state.backwardSteps, nextBackwardStep);
            if (!forwardExists && !backwardExists) {
                return null;
            }
            if (!backwardExists) {
                if (explore!true(nextForwardStep)) {
                    return result;
                }
            } else if (!forwardExists || nextForwardStep.distance > nextBackwardStep.distance) {
                if (explore!false(nextBackwardStep)) {
                    return result;
                }
            } else {
                if (explore!true(nextForwardStep)) {
                    return result;
                }
            }
        }
    }

    private static bool getNextStep(Step[ProcessedType] steps, out Step result) {
        auto unexplored = steps.byValue.filter!(a => !a.explored);
        if (unexplored.empty) {
            return false;
        }
        result = unexplored.minElement!(a => a.distance);
        return true;
    }

    private static ResultT buildPath(SearchState state, ProcessedType connection) {
        ResultT result = [];
        ProcessedType iterator = connection;
        while (true) {
            Step step = state.forwardSteps[iterator];
            if (step.predecessor is null) {
                break;
            }
            iterator = step.predecessor;
            result ~= step.function_;
        }
        iterator = connection;
        while (true) {
            Step step = state.backwardSteps[iterator];
            if (step.predecessor is null) {
                break;
            }
            iterator = step.predecessor;
            result = [step.function_] ~ result;
        }
        return result;
    }

    private static FunctionDeclaration toFunctionDeclaration(ProcessedValue processed) {
        return dispatch((ProcessedFunctionValue asFunction) {
            return asFunction.declaration;
        })(processed);
    }

    private static struct Edge {
        ProcessedType target;
        FunctionDeclaration function_;
    }

    private Edge[] getOutgoingEdges(ProcessedType source) {
        Edge[] result = [];
        foreach (key, value; source.metadata) {
            if (auto asImplicitTo = cast(ProcessedImplicitToMDKType) key) {
                result ~= Edge(asImplicitTo.targetType, toFunctionDeclaration(value));
            }
        }
        return result;
    }

    private Edge[] getIncomingEdges(ProcessedType target) {
        Edge[] result = [];
        foreach (key, value; target.metadata) {
            if (auto asImplicitFrom = cast(ProcessedImplicitFromMDKType) key) {
                result ~= Edge(asImplicitFrom.sourceType, toFunctionDeclaration(value));
            }
        }
        return result;
    }

}
