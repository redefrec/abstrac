module abstrac.codegen.overloads;

import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.llvm.modules;
import abstrac.templates.instantiation;
import abstrac.processed_types;
import abstrac.type_processor;
import std.array, std.algorithm, std.variant;

struct Overloads {
    FunctionDeclaration[] functions;
    TemplateBinding[] templates;

    this(OverloadSet node) {
        functions = node.functions;
        templates = node.templates.map!(a => TemplateBinding(a, 0, false, [])).array;
    }

    this(TemplateBinding[] templates) {
        this.templates = templates;
    }
}

private alias OverloadedElement = Variant;

struct OverloadResolver {
    ModuleBuilder mBuilder;
    Overloads overloads;
    AAstNode contextNode;
    ProcessedType[] argTypes;

    private ProcessedType[] getParamTypes(FunctionDeclaration function_) {
        auto typeNodes = pathSelect!(FunctionParameter, Type)(function_);
        return typeNodes.map!(a => TypeProcessor(mBuilder).process(a)).array;
    }

    private ProcessedType[] getParamTypes(TemplateBinding binding) {
        return TemplateInstantiator(mBuilder).getFunctionParamTypes(binding);
    }

    private bool isViable(FunctionDeclaration function_) {
        bool variadic = tryGetUniqueChild!VarArgsFunctionParameter(function_) !is null;
        auto params = getParamTypes(function_);
        if (params.length > argTypes.length || (!variadic && argTypes.length > params.length)) {
            return false;
        }
        if (argTypes[0 .. params.length] != params) {
            return false;
        }
        return true;
    }

    private bool isViable(TemplateBinding binding) {
        if (!binding.complete) {
            return false;
        }
        try {
            auto instance = TemplateInstantiator(mBuilder).instantiate(binding);
            auto function_ = cast(FunctionDeclaration) instance;
            return function_ !is null && isViable(function_);
        } catch (TemplatingException) {
            return false;
        }
    }

    private int compare(FunctionDeclaration left, FunctionDeclaration right) {
        auto leftExact = getParamTypes(left) == argTypes;
        auto rightExact = getParamTypes(right) == argTypes;
        if (leftExact != rightExact) {
            return leftExact ? -1 : 1;
        }
        assert(0);
    }

    private int compare(TemplateBinding left, TemplateBinding right) {
        if (left.complete != right.complete) {
            return left.complete ? -1 : 1;
        }
        assert(left.complete && right.complete);
        auto leftExact = getParamTypes(left) == argTypes;
        auto rightExact = getParamTypes(right) == argTypes;
        if (leftExact != rightExact) {
            return leftExact ? -1 : 1;
        }
        return 0;
    }

    private int compare(TemplateBinding left, FunctionDeclaration right) {
        if (getParamTypes(right) == argTypes) {
            return 1;
        }
        if (left.specs > 0) {
            return -1;
        }
        return 1;
    }

    private int compare(Second)(OverloadedElement first, Second second) {
        if (auto function_ = first.peek!FunctionDeclaration) {
            if (*function_ is null) {
                return 1;
            }
            return -compare(second, *function_);
        } else if (auto binding = first.peek!TemplateBinding) {
            return compare(*binding, second);
        } else {
            assert(0);
        }
    }

    private FunctionDeclaration toFunctionEponym(AAstNode node) {
        import abstrac.utils.multi_dispatch : dispatch;

        FunctionDeclaration result;
        dispatch((TemplateInstance arg) {
            result = toFunctionEponym(getEponym(arg));
        }, (FunctionDeclaration arg) { result = arg; })(node);
        return result;
    }

    FunctionDeclaration resolve() {
        auto viable = overloads.functions.filter!(a => isViable(a)).array;
        auto instantiator = TemplateInstantiator(mBuilder);
        auto viableTemplates = instantiator.deduce(overloads.templates, argTypes);
        OverloadedElement best = cast(FunctionDeclaration) null;
        foreach (function_; viable) {
            if (compare(best, function_) > 0) {
                best = function_;
            }
        }
        foreach (binding; viableTemplates) {
            if (compare(best, binding) > 0) {
                best = binding;
            }
        }
        if (auto function_ = best.peek!FunctionDeclaration) {
            if (*function_ is null) {
                printAst(contextNode);
                throw new Exception("Couldn't resolve overloads!");
            }
            return *function_;
        } else if (auto binding = best.peek!TemplateBinding) {
            return toFunctionEponym(TemplateInstantiator(mBuilder).instantiate(*binding));
        } else {
            assert(0);
        }
    }
}
