module abstrac.codegen.literals;

import abstrac.ast;
import abstrac.llvm.modules;
import abstrac.processed_values;
import llvm;
import std.bigint;

struct IntegerLiteral {
    ModuleBuilder mBuilder;

    ProcessedType getTypeOf(DecIntegerLiteral _) {
        return null;
    }

    LLVMValueRef getLLVMValue(DecIntegerLiteral _) {
        return null;
    }

}
