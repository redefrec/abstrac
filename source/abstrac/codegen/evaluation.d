module abstrac.codegen.evaluation;

import abstrac.llvm.modules : ModuleBuilder, IModuleCache;
import abstrac.processed_values;
import abstrac.ast.makers;
import abstrac.ast.helpers;
import abstrac.ast;
import llvm;

private class Cache : IModuleCache {
    ProcessedValue[AAstNode] evaluations;
    LLVMExecutionEngineRef interpreter;
    FunctionDeclaration[void* ] functions;
    size_t functionCounter;
}

struct Evaluator {
    ModuleBuilder mBuilder;

    this(ModuleBuilder mBuilder) {
        this.mBuilder = mBuilder;
    }

    private @property Cache cache() {
        return mBuilder.getOrCreateCache!Cache();
    }

    void registerFunction(FunctionDeclaration declaration, LLVMValueRef llvmValue) {
        import std.string : toStringz;

        auto extractorReturnType = LLVMPointerType(LLVMInt8Type(), 0);
        auto extractorType = LLVMFunctionType(extractorReturnType, null, 0, 0);
        auto addressExtractorFn = LLVMAddFunction(mBuilder.module_,
                toStringz(declaration.value ~ "_$_address_extraction"), extractorType);
        auto builder = LLVMCreateBuilder();
        auto entry = LLVMAppendBasicBlock(addressExtractorFn, "entry");
        LLVMPositionBuilderAtEnd(builder, entry);
        LLVMBuildRet(builder, LLVMBuildBitCast(builder, llvmValue, extractorReturnType, ""));
        LLVMDisposeBuilder(builder);
        auto functionAddressGv = LLVMRunFunction(getInterpreter(), addressExtractorFn, 0, null);
        LLVMDeleteFunction(addressExtractorFn);
        auto ptr = LLVMGenericValueToPointer(functionAddressGv);
        cache.functions[ptr] = declaration;
    }

    private ProcessedValue toProcessedValue(LLVMGenericValueRef generic, ProcessedType asType) {
        import abstrac.utils.multi_dispatch : dispatch, tryDispatch;
        import std.bigint : BigInt;
        import std.conv : to;

        auto result = dispatch((ProcessedIntegerType arg) {
            ulong raw = LLVMGenericValueToInt(generic, arg.signed ? 1 : 0);
            return ProcessedIntegerValue.get(arg, arg.signed ? BigInt(cast(long) raw) : BigInt(raw));
        }, (ProcessedFunctionType arg) {
            auto lookup = LLVMGenericValueToPointer(generic) in cache.functions;
            assert(lookup !is null);
            return ProcessedFunctionValue.get(arg, *lookup);
        }, (ProcessedPointerType arg) {
            if (auto ubyteType = cast(ProcessedIntegerType) arg.pointedType) {

                assert(ubyteType.bitWidth == 8 && !ubyteType.signed,
                    "Cannot evaluate pointers other than ubyte* at compile-time!");
                auto ptr = cast(const(char)*) LLVMGenericValueToPointer(generic);
                return ProcessedStringValue.get(to!string(ptr));
            } else {
                assert(0, "Cannot evaluate pointers other than ubyte* at compile-time!");
            }
        })(asType);
        return result;
    }

    private LLVMExecutionEngineRef getInterpreter() {
        if (cache.interpreter is null) {
            assert(!LLVMCreateInterpreterForModule(&cache.interpreter, mBuilder.module_, null));
        }
        return cache.interpreter;
    }

    private string nextFunctionName() {
        import std.conv : to;

        return "__compiletime_evaluation_" ~ to!string(cache.functionCounter++);
    }

    ProcessedValue evaluate(AAstNode arg, ProcessedType type = null) {
        import abstrac.type_processor;

        // TODO: find a better way to preserve parent
        if (auto cached = arg in cache.evaluations) {
            return *cached;
        }
        type = type is null ? TypeProcessor(mBuilder).processExpression(arg) : type;
        Type returnTypeNode = makeType(type);
        AAstNode clone = arg.clone();
        Statement retStmt = makeReturnStatement(clone);
        clone.parent = arg.parent;
        FunctionDeclaration funDecl = makeFunctionDeclaration(nextFunctionName(),
                returnTypeNode, [], [retStmt]);
        append(mBuilder.unit, funDecl);
        auto function_ = mBuilder.requireFunction(funDecl);
        clone.parent = null;
        LLVMGenericValueRef genericValue = LLVMRunFunction(getInterpreter(), function_, 0, null);
        remove(funDecl);
        LLVMDeleteFunction(function_);
        auto result = toProcessedValue(genericValue, type);
        LLVMDisposeGenericValue(genericValue);
        cache.evaluations[arg] = result;
        return result;
    }
}
