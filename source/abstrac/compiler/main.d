module abstrac.compiler.main;

import llvm;

void main() {
    import abstrac.ast.helpers : printAst;
    import abstrac.manipulation : applyManipulations;
    import abstrac.llvm.modules : buildModule;

    auto units = parse(["./samples/main.ac"], ["./samples"]);
    applyManipulations(units);
    // printAst(units["std.traits"]);
    auto module_ = buildModule(units["main"]);
    LLVMDumpModule(module_);
    LLVMVerifyModule(module_, LLVMPrintMessageAction, null);
}

private auto parse(string[] files, string[] folders) {
    import abstrac.parser : Parser;

    Parser parser = new Parser(files, folders);
    return parser.parseFiles();
}
