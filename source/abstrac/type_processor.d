module abstrac.type_processor;

import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.llvm.modules;
import abstrac.processed_values;
import abstrac.utils.multi_dispatch : dispatch;
import abstrac.utils.visitor : TVisitor;
import abstrac.utils.stack : Stack;
import std.array, std.algorithm;

private class Cache : IModuleCache {
    ProcessedType[Type] typeNodeMap;

    ProcessedType[AAstNode] expressionNodeMap;

    ProcessedStructType[StructDeclaration] structDeclMap;
}

struct TypeProcessor {
    ModuleBuilder mBuilder;

    this(ModuleBuilder mBuilder) {
        this.mBuilder = mBuilder;
    }

    private @property Cache cache() {
        return mBuilder.getOrCreateCache!Cache();
    }

    ProcessedStructType processDeclaration(StructDeclaration arg) {
        if (auto cached = arg in cache.structDeclMap) {
            return *cached;
        }
        auto structBody = tryGetUniqueChild!StructDeclarationBody(arg);
        ProcessedStructType result;
        if (structBody is null) {
            result = ProcessedStructType.get(arg, null);
        } else {
            Type[] typeNodes = pathSelect!Type(structBody);
            ProcessedType[] types = typeNodes.map!(a => process(a)).array;
            result = ProcessedStructType.get(arg, types);
        }
        cache.structDeclMap[arg] = result;
        return result;
    }

    ProcessedFunctionType processDeclaration(FunctionDeclaration arg) {
        VarArgsFunctionParameter varArgsNode = tryGetUniqueChild!VarArgsFunctionParameter(arg);
        Type returnTypeNode = getUniqueChild!Type(arg);
        ProcessedType returnType = process(returnTypeNode);
        Type[] paramTypeNodes = pathSelect!(FunctionParameter, Type)(arg);
        ProcessedType[] paramTypes = paramTypeNodes.map!(a => process(a)).array;
        return ProcessedFunctionType.get(returnType, paramTypes, varArgsNode !is null);
    }

    ProcessedType process(Type arg) {
        if (auto entry = arg in cache.typeNodeMap) {
            return *entry;
        }
        TypeVisitor visitor = new TypeVisitor(&this);
        arg.accept(visitor);
        auto result = visitor.stack.popLast();
        cache.typeNodeMap[arg] = result;
        return result;
    }

    ProcessedType processExpression(AAstNode arg) {
        if (auto entry = arg in cache.expressionNodeMap) {
            return *entry;
        }
        ExpressionVisitor visitor = new ExpressionVisitor(&this);
        arg.accept(visitor);
        auto result = visitor.stack.popLast();
        cache.expressionNodeMap[arg] = result;
        return result;
    }

    private class TypeVisitor : TVisitor!true {
        Stack!ProcessedType stack;
        TypeProcessor* owner;

        this(TypeProcessor* owner) {
            this.owner = owner;
        }

        override void visit(Type arg) {
            visitChildren(arg);
        }

        private void visitIdentifiedDeclaration(AAstNode arg) {
            import abstrac.utils.multi_dispatch : dispatch;

            dispatch((AliasDeclaration asAlias) {
                auto typeNode = getUniqueChild!Type(asAlias);
                typeNode.accept(this);
            }, (StructDeclaration asStruct) {
                stack.push(owner.processDeclaration(asStruct));
            }, (TemplateInstance asInstance) {
                visitIdentifiedDeclaration(getEponym(asInstance));
            })(arg);
        }

        override void visit(BoundIdentifier arg) {
            visitIdentifiedDeclaration(arg.declaration);
        }

        override void visit(BuiltinType arg) {
            import abstrac.codegen.evaluation : Evaluator;

            final switch (arg.value) {
            case "void":
                stack.push(ProcessedVoidType.get());
                break;
            case "int":
            case "uint":
            case "real":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                auto bwExpr = getUniqueChild!Expression(instantiation);
                auto bwEval = Evaluator(owner.mBuilder).evaluate(bwExpr);
                uint bwHost = toHostValue!uint(bwEval);
                if (arg.value == "int") {
                    stack.push(ProcessedIntegerType.get(true, bwHost));
                } else if (arg.value == "uint") {
                    stack.push(ProcessedIntegerType.get(false, bwHost));
                } else {
                    stack.push(ProcessedRealType.get(bwHost));
                }
                break;
            case "setmod":
                Type[] typeNodes = pathSelect!(TemplateInstantiation, Type)(arg);
                auto left = owner.process(typeNodes[0]);
                auto right = owner.process(typeNodes[1]);
                stack.push(left.setModifier(right));
                break;
            case "setmetadata": {
                    import abstrac.codegen.evaluation : Evaluator;

                    auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                    auto base = cast(Type) instantiation.child(0);
                    auto key = cast(Type) instantiation.child(1);
                    auto expression = cast(Expression) instantiation.child(2);
                    assert(base !is null && key !is null && expression !is null);
                    auto processedBase = owner.process(base);
                    auto processedKey = owner.process(key);
                    auto evaluated = Evaluator(owner.mBuilder).evaluate(expression);
                    stack.push(processedBase.setMetaData(processedKey, evaluated));
                    break;
                }
            case "ptr":
                auto instantiation = onlyChild(arg);
                auto subtype = cast(Type) onlyChild(instantiation);
                subtype.accept(this);
                stack.push(ProcessedPointerType.get(stack.pop()));
                break;
            case "array":
                auto instantiation = onlyChild(arg);
                auto subtype = cast(Type) instantiation.child(0);
                auto size = cast(Expression) instantiation.child(1);
                subtype.accept(this);
                size_t hostSize = toHostValue!size_t(Evaluator(owner.mBuilder).evaluate(size));
                stack.push(ProcessedArrayType.get(hostSize, stack.pop()));
                break;
            case "tuple":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                ProcessedType[] memberTypes = [];
                foreach (child; instantiation.children) {
                    auto asType = cast(Type) child;
                    assert(asType !is null);
                    memberTypes ~= owner.process(asType);
                }
                stack.push(ProcessedTupleType.get(memberTypes));
                break;
            case "typeof":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                auto expression = onlyChild(instantiation);
                stack.push(owner.processExpression(expression));
                break;
            case "function":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                instantiation.child(0).accept(this);
                auto returnType = stack.pop();
                ProcessedType[] paramTypes = [];
                foreach (child; instantiation.children[1 .. $]) {
                    child.accept(this);
                    paramTypes ~= stack.pop();
                }
                stack.push(ProcessedFunctionType.get(returnType, paramTypes, false));
                break;
            case "implicit_to":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                auto targetType = owner.process(getUniqueChild!Type(instantiation));
                stack.push(ProcessedImplicitToMDKType.get(targetType));
                break;
            case "implicit_from":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                auto sourceType = owner.process(getUniqueChild!Type(instantiation));
                stack.push(ProcessedImplicitFromMDKType.get(sourceType));
                break;
            case "literal":
                stack.push(ProcessedLiteralModType.get());
                break;
            case "common": {
                    import abstrac.codegen.conversion : Conversion;

                    auto types = pathSelect!(TemplateInstantiation, Type)(arg);
                    assert(types.length == 2);
                    stack.push(Conversion(owner.mBuilder)
                            .common(owner.process(types[0]), owner.process(types[1])));
                    break;
                }
            }
        }

        override void visit(TemplateInstantiation arg) {
            import abstrac.templates.instantiation;

            auto instantiator = TemplateInstantiator(owner.mBuilder);
            auto result = instantiator.instantiate(arg);
            visitIdentifiedDeclaration(instantiator.finalize(result));
        }

        override void visit(ProcessedTypeNode arg) {
            stack.push(arg.processedType);
        }

    }

    private class ExpressionVisitor : TVisitor!true {
        Stack!ProcessedType stack;
        TypeProcessor* owner;

        this(TypeProcessor* owner) {
            this.owner = owner;
        }

        import std.typecons : BlackHole;
        import abstrac.codegen.overloads;

        private class OverloadSetType : BlackHole!ProcessedType {
            Overloads overloads;
            this(Overloads overloads) {
                this.overloads = overloads;
            }
        }

        private void visitIdentifiedDeclaration(AAstNode arg) {
            import abstrac.ast.makers : makeOverloadSet;

            ProcessedType result;
            dispatch((FunctionDeclaration arg) {
                result = owner.processDeclaration(arg);
            }, (TemplateDeclaration arg) {
                result = new OverloadSetType(Overloads(makeOverloadSet([arg])));
            }, (OverloadSet arg) { result = new OverloadSetType(Overloads(arg)); },
                    (PrecomputeDeclaration arg) {
                        result = owner.process(getUniqueChild!Type(arg));
                    }, (FunctionParameter arg) {
                        result = owner.process(getUniqueChild!Type(arg));
                    }, (TemplateInstance arg) {
                        visitIdentifiedDeclaration(getEponym(arg));
                        result = stack.pop();
                    }, (StaticSingleAssignmentStatement arg) {
                        result = owner.processExpression(onlyChild(arg));
                    })(arg);

            stack.push(result);
        }

        override void visit(BoundIdentifier arg) {
            visitIdentifiedDeclaration(arg.declaration);
        }

        override void visit(ProcessedValueNode arg) {
            stack.push(arg.processedValue.getType());
        }

        override void visit(CharacterLiteral arg) {
            stack.push(ProcessedPointerType.get(ProcessedIntegerType.get(false, 8)));
        }

        override void visit(DecIntegerLiteral arg) {
            import std.bigint : BigInt;

            auto base = ProcessedIntegerType.get(false, 64);
            auto plm = ProcessedLiteralModType.get();
            auto eval = ProcessedIntegerValue.get(base, BigInt(arg.value));
            stack.push(base.setModifier(plm).setMetaData(plm, eval));
        }

        override void visit(PostfixCallOperator arg) {
            auto callee = owner.processExpression(arg.lastChild());
            dispatch((OverloadSetType asOverloadSet) {
                ProcessedType[] argTypes = [];
                for (size_t i = 0; i < arg.children.length - 1; ++i) {
                    argTypes ~= owner.processExpression(arg.child(i));
                }
                auto resolver = OverloadResolver(owner.mBuilder,
                    asOverloadSet.overloads, arg, argTypes);
                auto functionType = owner.processDeclaration(resolver.resolve());
                stack.push(functionType.returnType);
            }, (ProcessedFunctionType asFunction) {
                stack.push(asFunction.returnType);
            })(callee);
        }

        override void visit(TemplateInstantiation arg) {
            import abstrac.templates.instantiation;

            auto instantiator = TemplateInstantiator(owner.mBuilder);
            auto result = instantiator.instantiate(arg);
            if (result.length != 1 || !result[0].complete) {
                stack.push(new OverloadSetType(Overloads(result)));
                return;
            }
            visitIdentifiedDeclaration(instantiator.finalize(result));
        }

        override void visit(Expression arg) {
            visitChildren(arg);
        }

        override void visit(BuiltinExpression arg) {
            final switch (arg.value) {
            case "cmp_eq":
            case "cmp_ne":
            case "cmp_gt":
            case "cmp_ge":
            case "cmp_lt":
            case "cmp_le":
                stack.push(ProcessedIntegerType.get(false, 1));
                break;
            case "bitcast":
            case "trunc":
            case "fptrunc":
            case "zeroext":
            case "signext":
            case "fpext":
            case "fptoui":
            case "fptosi":
            case "uitofp":
            case "sitofp":
            case "ptrtoint":
            case "inttoptr":
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                assert(instantiation.children.length == 1);
                auto targetTypeNode = getUniqueChild!Type(instantiation);
                stack.push(owner.process(targetTypeNode));
                break;
            case "shl":
            case "lshr":
            case "ashr":
            case "and":
            case "or":
            case "xor":
            case "add":
            case "sub":
            case "mul":
            case "div":
            case "rem":
                auto call = getUniqueChild!PostfixCallOperator(arg);
                assert(call.children.length == 2);
                auto type = owner.processExpression(call.child(0));
                stack.push(type);
                break;
            case "alloca":
                auto typeNode = getUniqueChild!Type(onlyChild(arg));
                stack.push(ProcessedPointerType.get(owner.process(typeNode)));
                break;
            case "load":
                auto call = getUniqueChild!PostfixCallOperator(arg);
                auto lhs = owner.processExpression(call.child(0));
                dispatch((ProcessedPointerType ptrType) {
                    stack.push(ptrType.pointedType);
                })(lhs);
                break;
            case "store":
                stack.push(ProcessedVoidType.get());
                break;
            case "gep":
                import std.array : array;

                auto call = getUniqueChild!PostfixCallOperator(arg);
                auto aggregate = owner.processExpression(call.child(0));
                ProcessedType resultType = getGepResultType(aggregate, call.children[1 .. $].array);
                stack.push(ProcessedPointerType.get(resultType));
                break;
            }
        }

        private ProcessedType getGepResultType(ProcessedType type,
                AAstNode[] indexes, size_t idxidx = 0) {
            if (idxidx >= indexes.length) {
                return type;
            }
            auto idx = indexes[idxidx++];
            ProcessedType result;
            dispatch((ProcessedPointerType asPtr) {
                result = getGepResultType(asPtr.pointedType, indexes, idxidx);
            }, (ProcessedArrayType asArray) {
                result = getGepResultType(asArray.elementType, indexes, idxidx);
            }, (ProcessedStructType asStruct) {
                import abstrac.codegen.evaluation : Evaluator;

                // TODO: indexType shouldn't be restricted to 32bit
                // (but is because struct indices must be i32)
                auto evaluation = Evaluator(owner.mBuilder).evaluate(idx,
                    ProcessedIntegerType.get(true, 32));
                size_t index = toHostValue!size_t(evaluation);
                result = asStruct.memberTypes[index];
            })(type);
            return result;
        }

    }

}

private T toHostValue(T)(ProcessedValue arg) {
    import std.conv : to;

    T result;
    dispatch((ProcessedIntegerValue arg) {
        assert(__traits(isIntegral, T));
        result = to!T(arg.value);
    })(arg);
    return result;
}
