module abstrac.ast.makers;

import abstrac.ast;
import abstrac.ast.helpers;
import abstrac.utils.multi_dispatch;
import abstrac.processed_values;
import std.conv : to;
import core.stdc.stdarg;

auto makePlaceholder() {
    return new CustomAstNode;
}

OverloadSet makeOverloadSet(Range)(Range nodes) {
    OverloadSet result = new OverloadSet;
    foreach (node; nodes) {
        if (auto asTemplate = cast(TemplateDeclaration) node) {
            result.templates ~= asTemplate;
        } else if (auto asFunction = cast(FunctionDeclaration) node) {
            result.functions ~= asFunction;
        } else {
            throw new Exception("Invalid multiple declarations with name: " ~ node.value);
        }
    }
    return result;
}

Expression makeExpression(ProcessedValue arg) {
    ProcessedValueNode child = new ProcessedValueNode;
    child.processedValue = arg;
    Expression result = new Expression;
    moveChild(child, result);
    return result;
}

Statement makeReturnStatement(AAstNode expression) {
    Statement result = new Statement;
    ReturnStatement ret = new ReturnStatement;
    moveChild(expression, ret);
    moveChild(ret, result);
    return result;
}

Type makeType(ProcessedType arg) {
    Type result = new Type;
    AAstNode child = makeTypeChild(arg);
    moveChild(child, result);
    return result;
}

ProcessedTypeNode makeTypeChild(ProcessedType typeClass) {
    ProcessedTypeNode result = new ProcessedTypeNode;
    result.processedType = typeClass;
    return result;
}

TemplateDeclaration makeTemplateDeclaration(TemplateParameterList tmpParamList, AAstNode node) {
    TemplateDeclaration result = new TemplateDeclaration;
    result.value = node.value;
    node.value = "";
    moveChild(tmpParamList, result);
    moveChild(node, result);
    return result;
}

FunctionDeclaration makeFunctionDeclaration(string name, Type type,
        FunctionParameter[] params, Statement[] statements) {
    FunctionDeclaration result = new FunctionDeclaration;
    result.value = name;
    moveChild(type, result);
    foreach (param; params) {
        moveChild(param, result);
    }
    if (statements !is null) {
        FunctionBody functionBody = new FunctionBody;
        foreach (statement; statements) {
            moveChild(statement, functionBody);
        }
        moveChild(functionBody, result);
    }
    return result;
}

PostfixCallOperator makePostfixCallOperator(AAstNode[] arguments, AAstNode callee) {
    PostfixCallOperator result = new PostfixCallOperator;
    foreach (argument; arguments) {
        moveChild(argument, result);
    }
    moveChild(callee, result);
    return result;
}

@("extension_usable") extern (C) {
    Identifier makeIdentifier(const(char)* value) {
        auto result = new Identifier;
        result.value = to!string(value);
        return result;
    }

    BoundIdentifier makeBoundIdentifier(AAstNode declaration) {
        assert(declaration !is null);
        BoundIdentifier result = new BoundIdentifier;
        result.value = declaration.value;
        result.declaration = declaration;
        return result;
    }

    CustomInfixOperator makeCustomInfixOperator(const(char)* name,
            const(char)* associativity, long precedence) {
        CustomInfixOperator result = new CustomInfixOperator;
        result.value = to!string(name);
        final switch (to!string(associativity)) {
        case "left":
            result.leftAssociative = true;
            break;
        case "right":
            result.leftAssociative = false;
            break;
        }
        result.precedence = precedence;
        return result;
    }

    StaticSingleAssignmentStatement makeStaticSingleAssignment(const(char)* name, AAstNode expr) {
        auto result = new StaticSingleAssignmentStatement;
        result.value = to!string(name);
        moveChild(wrapped!Expression(expr), result);
        return result;
    }

    TemplateInstantiation makeTemplateInstantiationOfParameters(TemplateParameterList arg) {
        import std.string : toStringz;

        TemplateInstantiation result = new TemplateInstantiation;
        foreach (child; arg.children) {
            dispatch((TemplateTypeParameter asTypeParam) {
                moveChild(wrapped!Type(makeIdentifier(toStringz(asTypeParam.value))), result);
            }, (TemplateValueParameter asValueParam) {
                moveChild(wrapped!Expression(makeIdentifier(toStringz(asValueParam.value))),
                    result);
            })(child);
        }
        return result;
    }

    AAstNode[] makeArray1(AAstNode arg1) {
        return [arg1];
    }

    AAstNode[] makeArray2(AAstNode arg1, AAstNode arg2) {
        return [arg1, arg2];
    }

    AAstNode[] makeArray3(AAstNode arg1, AAstNode arg2, AAstNode arg3) {
        return [arg1, arg2, arg3];
    }

    ScopeGuardStatement makeScopeGuardStatement(AAstNode[] statements) {
        ScopeGuardStatement result = new ScopeGuardStatement;
        foreach (statement; statements) {
            moveChild(wrapped!Statement(statement), result);
        }
        return result;
    }

    StaticIfStatement makeStaticIfStatement(AAstNode condition, AAstNode[] statements) {
        StaticIfStatement result = new StaticIfStatement;
        moveChild(wrapped!Expression(condition), result);
        StaticIfStatementThenBlock thenBlock = new StaticIfStatementThenBlock;
        foreach (statement; statements) {
            moveChild(wrapped!Statement(statement), thenBlock);
        }
        moveChild(thenBlock, result);
        return result;
    }

    Statement makeExpressionStatement(AAstNode expr) {
        Statement result = new Statement;
        ExpressionStatement exprStmt = new ExpressionStatement;
        moveChild(wrapped!Expression(expr), exprStmt);
        moveChild(exprStmt, result);
        return result;
    }

    Type makeType(AAstNode arg) {
        Type result = new Type;
        moveChild(arg, result);
        return result;
    }

    Expression makeExpression(AAstNode child) {
        Expression result = new Expression;
        moveChild(child, result);
        return result;
    }

    BuiltinType makeArrayType(AAstNode type, AAstNode size) {
        return makeBuiltinType("array", [wrapped!Type(type), wrapped!Expression(size)]);
    }

    BuiltinType makePointerType(AAstNode type) {
        return makeBuiltinType("ptr", [wrapped!Type(type)]);
    }

    BuiltinType makeTypeof(AAstNode expr) {
        return makeBuiltinType("typeof", [wrapped!Expression(expr)]);
    }

    BuiltinType makeSetMod(AAstNode base, AAstNode modifier) {
        return makeBuiltinType("setmod", [wrapped!Type(base), wrapped!Type(modifier)]);
    }

    BuiltinType makeSetMetaData(AAstNode base, AAstNode key, AAstNode value) {
        return makeBuiltinType("setmetadata", [wrapped!Type(base),
                wrapped!Type(key), wrapped!Expression(value)]);
    }

    BuiltinExpression makeHasMetaData(AAstNode base, AAstNode key) {
        return makeBuiltinExpression("hasmetadata", [wrapped!Type(base), wrapped!Type(key)], []);
    }

    BuiltinExpression makeGetMetaData(AAstNode base, AAstNode key) {
        return makeBuiltinExpression("getmetadata", [wrapped!Type(base), wrapped!Type(key)], []);
    }

    BuiltinExpression makeAlloca(AAstNode type) {
        return makeBuiltinExpression("alloca", [wrapped!Type(type)], []);
    }

    BuiltinExpression makeTypecast(const(char)* proc, AAstNode type, AAstNode expr) {
        return makeBuiltinExpression(proc, [wrapped!Type(type)], [wrapped!Expression(expr)]);
    }

    BuiltinExpression makeStore(AAstNode target, AAstNode value) {
        return makeBuiltinExpression("store", [], [wrapped!Expression(target),
                wrapped!Expression(value)]);
    }

    BuiltinExpression makeLoad(AAstNode address) {
        return makeBuiltinExpression("load", [], [wrapped!Expression(address)]);
    }

    DecIntegerLiteral makeDecIntegerLiteral(const(char)* value) {
        DecIntegerLiteral literal = new DecIntegerLiteral;
        literal.value = to!string(value);
        return literal;
    }

    CharacterLiteral makeStringLiteral(const(char)* value) {
        CharacterLiteral literal = new CharacterLiteral;
        literal.value = to!string(value);
        return literal;
    }

    CustomAstNode newCustom(const(char)* name) {
        CustomAstNode result = new CustomAstNode;
        result.name = to!string(name);
        return result;
    }
}

private T wrapped(T)(AAstNode arg) {
    if (auto asT = cast(T) arg) {
        return asT;
    }
    T result = new T;
    moveChild(arg, result);
    return result;
}

private BuiltinType makeBuiltinType(string name, AAstNode[] params) {
    BuiltinType result = new BuiltinType;
    result.value = name;
    TemplateInstantiation instantiation = new TemplateInstantiation;
    moveChild(instantiation, result);
    foreach (param; params) {
        moveChild(param, instantiation);
    }
    return result;
}

private BuiltinExpression makeBuiltinExpression(const(char)* name,
        AAstNode[] templateParams, AAstNode[] callArguments) {
    BuiltinExpression result = new BuiltinExpression;
    result.value = to!string(name);
    if (templateParams.length > 0) {
        TemplateInstantiation instantiation = new TemplateInstantiation;
        moveChild(instantiation, result);
        foreach (param; templateParams) {
            moveChild(param, instantiation);
        }
    }
    if (callArguments.length > 0) {
        PostfixCallOperator call = new PostfixCallOperator;
        moveChild(call, result);
        foreach (arg; callArguments) {
            moveChild(arg, call);
        }
    }
    return result;
}
