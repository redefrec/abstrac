module abstrac.ast.grammar;

import abstrac.ast : TAstNode;

class GrammarFile : TAstNode!GrammarFile {
}

class Grammar : TAstNode!Grammar {
}

class GrammarTopRule : TAstNode!GrammarTopRule {
}

class GrammarTopNode : TAstNode!GrammarTopNode {
}

class GrammarFolding : TAstNode!GrammarFolding {
}

class GrammarTopSequence : TAstNode!GrammarTopSequence {
}

class GrammarTopChoice : TAstNode!GrammarTopChoice {
}

class GrammarModifier : TAstNode!GrammarModifier {
}

class GrammarSpacing : TAstNode!GrammarSpacing {
}

class GrammarTopListElement : TAstNode!GrammarTopListElement {
}

class GrammarPredefinedRule : TAstNode!GrammarPredefinedRule {
}

class GrammarNodeId : TAstNode!GrammarNodeId {
}

class GrammarDqStr : TAstNode!GrammarDqStr {
}

class GrammarSqStr : TAstNode!GrammarSqStr {
}

class GrammarKeyword : TAstNode!GrammarKeyword {
}
