module abstrac.ast;

public import abstrac.ast.grammar;
public import abstrac.ast.language.commons;
public import abstrac.ast.language.declarations;
public import abstrac.ast.language.extension;
public import abstrac.ast.language.expressions;
public import abstrac.ast.language.statements;
public import abstrac.ast.language.types;
import abstrac.utils.visitor : IVisitor, TVisitor;

package struct ClonedProperty;

abstract class AAstNode {
    import std.container.array : Array;

    AAstNode parent;
    AAstNode[] children;
    string value;
    private static size_t counter = 0;
    private size_t id;

    this() {
        this.id = counter++;
    }

    AAstNode child(size_t index) {
        assert(children.length > index);
        return children[index];
    }

    AAstNode lastChild() {
        assert(children.length > 0);
        return children[$ - 1];
    }

    override bool opEquals(Object arg) {
        AAstNode asNode = cast(AAstNode) arg;
        return asNode !is null && id == asNode.id;
    }

    override size_t toHash() {
        return id;
    }

    abstract void accept(IVisitor);
    abstract AAstNode clone(AAstNode[AAstNode] declarationReplacements = null);
}

abstract class TAstNode(T) : AAstNode {
    override void accept(IVisitor visitor) {
        visitor.visit(cast(T) this);
    }

    override string toString() const {
        import std.conv : to;

        return "#" ~ to!string(id) ~ " " ~ T.stringof ~ (value != "" ? " (" ~ value ~ ")" : "");
    }

    override AAstNode clone(AAstNode[AAstNode] declarationReplacements = null) {
        import std.meta : staticIndexOf;
        import std.traits : getSymbolsByUDA;

        if (declarationReplacements is null) {
            declarationReplacements[null] = null;
            declarationReplacements.remove(null);
        }
        T result = new T;
        result.value = this.value;
        static foreach (symbol; getSymbolsByUDA!(T, ClonedProperty)) {

            mixin("result." ~ __traits(identifier,
                    symbol) ~ " = (cast(T)this)." ~ __traits(identifier, symbol) ~ ";");
        }
        static if (staticIndexOf!(T, Nodes.Declarations)) {
            declarationReplacements[this] = result;
        }
        foreach (child; this.children) {
            auto childClone = child.clone(declarationReplacements);
            childClone.parent = result;
            result.children ~= childClone;
        }

        class BoundIdentifierVisitor : TVisitor!false {
            override void visit(BoundIdentifier arg) {
                if (auto found = arg.declaration in declarationReplacements) {
                    arg.declaration = *found;
                }
            }
        }

        result.accept(new BoundIdentifierVisitor);
        return result;
    }
}

struct Nodes {
    import std.meta : Alias, AliasSeq, ApplyLeft, staticMap, Filter;

    private alias NameToAstNode(alias mod, alias string name) = Alias!(
            __traits(getMember, mod, name));
    private enum isStringAstNode(alias mod, alias string name) = is(NameToAstNode!(mod,
                name) : AAstNode);

    private template ModuleNodes(alias mod) {
        alias NodeNames = Filter!(ApplyLeft!(isStringAstNode, mod), __traits(allMembers, mod));
        alias ModuleNodes = staticMap!(ApplyLeft!(NameToAstNode, mod), NodeNames);
    }

    alias All = AliasSeq!(Language, Nodes.Grammar);
    alias Language = AliasSeq!(ModuleNodes!(abstrac.ast.language.commons),
            ModuleNodes!(abstrac.ast.language.declarations), ModuleNodes!(abstrac.ast.language.extension),
            ModuleNodes!(abstrac.ast.language.expressions),
            ModuleNodes!(abstrac.ast.language.statements),
            ModuleNodes!(abstrac.ast.language.types));
    alias Grammar = ModuleNodes!(abstrac.ast.grammar);

    alias Scopes = AliasSeq!(Unit, TemplateDeclaration, FunctionDeclaration, IfStatement,
            IfThenBlock, IfElseBlock, WhileLoopStatement, ExtensionManipulationVisit);

    alias Declarations = AliasSeq!(TemplateDeclaration, AliasDeclaration,
            PrecomputeDeclaration, FunctionDeclaration, StaticSingleAssignmentStatement,
            FunctionParameter, StructDeclaration, TemplateTypeParameter, TemplateValueParameter);

}
