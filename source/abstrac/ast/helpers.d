module abstrac.ast.helpers;

import abstrac.ast;
import std.string : toStringz;
import std.conv : to;

auto indexOf(AAstNode parent, AAstNode child) {
    static struct Result {
        size_t index;
        bool found;
    }

    size_t index = 0;
    foreach (element; parent.children) {
        if (element is child) {
            return Result(index, true);
        }
        ++index;
    }
    return Result(size_t.max, false);
}

bool isScope(AAstNode node) {
    static foreach (ScopeT; Nodes.Scopes) {
        if (cast(ScopeT) node) {
            return true;
        }
    }
    return false;
}

AAstNode getScopeNode(AAstNode arg) {
    import std.algorithm : canFind;

    while (arg !is null) {
        if (isScope(arg)) {
            return arg;
        }
        arg = arg.parent;
    }
    assert(0);
}

auto pathSelect(Node)(AAstNode root) {
    static assert(is(Node : AAstNode));

    Node[] result = [];
    foreach (child; root.children) {
        auto asRequested = cast(Node) child;
        if (asRequested !is null) {
            result ~= asRequested;
        }
    }
    return result;
}

auto pathSelect(NodePath...)(AAstNode root) {
    static assert(NodePath.length > 0);
    NodePath[$ - 1][] result = [];
    foreach (child; root.children) {
        if ((cast(NodePath[0]) child) !is null) {
            result ~= pathSelect!(NodePath[1 .. $])(child);
        }
    }
    return result;
}

auto findAncestorByType(Node)(AAstNode arg) {
    AAstNode current = arg.parent;
    while (current !is null) {
        Node asNode = cast(Node) current;
        if (asNode !is null) {
            return asNode;
        }
        current = current.parent;
    }
    return null;
}

AAstNode onlyChild(AAstNode arg) {
    assert(arg.children.length == 1);
    return arg.child(0);
}

AAstNode getEponym(TemplateInstance arg) {
    return arg.lastChild();
}

AAstNode getEponym(TemplateDeclaration arg) {
    return arg.lastChild();
}

long parseLongValue(AAstNode arg) {
    import abstrac.utils.multi_dispatch : dispatch;

    long result = 0;
    dispatch((NegatableDecIntegerLiteral arg) {
        result = parseLongValue(onlyChild(arg)) * (arg.value == "-" ? -1 : 1);
    }, (DecIntegerLiteral arg) { result = to!long(arg.value); })(arg);
    return result;
}

@("extension_usable") extern (C) {
    bool isNotNull(AAstNode arg) {
        return arg !is null;
    }

    AAstNode childAt(AAstNode parent, long index) {
        return parent.child(cast(size_t) index);
    }

    void append(AAstNode parent, AAstNode child) {
        assert(child.parent is null);
        child.parent = parent;
        parent.children ~= child;
    }

    void moveChild(AAstNode node, AAstNode newParent) {
        if (node.parent !is null) {
            remove(node);
        }
        append(newParent, node);
    }

    void insertAfter(AAstNode insertee, AAstNode position) {
        import std.array : insertInPlace;

        auto parent = position.parent;
        assert(parent !is null);
        assert(insertee.parent is null);
        insertee.parent = parent;
        const indexOfResult = indexOf(parent, position);
        assert(indexOfResult.found);
        const index = indexOfResult.index;
        parent.children.insertInPlace(index + 1, insertee);
    }

    void remove(AAstNode node) {
        import std.algorithm : remove;

        auto parent = node.parent;
        assert(parent !is null);
        for (size_t i = 0; i < parent.children.length; i++) {
            if (parent.child(i) is node) {
                parent.children = parent.children.remove(i);
                node.parent = null;
                return;
            }
        }
        assert(0);
    }

    void replace(AAstNode node, AAstNode replacement) {
        assert(node.parent !is null);
        assert(replacement.parent is null || replacement.parent is node);
        foreach (ref child; node.parent.children) {
            if (child is node) {
                replacement.parent = node.parent;
                child = replacement;
                node.parent = null;
                return;
            }
        }
        assert(0);
    }

    AAstNode clone(AAstNode arg) {
        return arg.clone();
    }

    void moveChildRange(AAstNode from, AAstNode to, long begin, long end) {
        foreach_reverse (i; begin .. end) {
            moveChild(from.child(cast(size_t) i), to);
        }
    }

    void printAst(AAstNode arg) {
        import abstrac.utils.visitor : TVisitor;
        import std.stdio : writeln;

        class LocalVisitor : TVisitor!false {
            private size_t mIndent = 0;

            private void indent() {
                mIndent += 2;
            }

            private void outdent() {
                mIndent -= 2;
            }

            private void print(Args...)(Args args) {
                import std.range : repeat;

                writeln(' '.repeat(mIndent), args);
            }

            static foreach (Node; Nodes.All) {
                override void visit(Node node) {
                    print(node);
                    indent();
                    super.visit(node);
                    outdent();
                }
            }
        }

        if (arg is null) {
            writeln("null");
        } else {
            arg.accept(new LocalVisitor);
        }
    }

    void setValue(AAstNode node, const(char)* val) { // @suppress(dscanner.suspicious.unused_parameter)
        node.value = to!string(val);
    }

    const(char)* valueOf(AAstNode arg) {
        return toStringz(arg.value);
    }

    const(char)* longToString(long arg) {
        return toStringz(to!string(arg));
    }

    long getLength(AAstNode[] arg) {
        return arg.length;
    }

    AAstNode at(AAstNode[] arg, long index) {
        return arg[cast(size_t) index];
    }

    const(char)* stringConcat(const(char)* left, const(char)* right) {
        return toStringz(to!string(left) ~ to!string(right));
    }

    AAstNode[] getAllCustom(const(char)* name, AAstNode arg) {
        import std.algorithm, std.array;

        string strName = to!string(name);
        return cast(AAstNode[]) pathSelect!CustomAstNode(arg).filter!(a => a.name == strName).array;
    }

    CustomAstNode getCustom(const(char)* name, AAstNode arg) {
        import std.algorithm, std.array;

        string strName = to!string(name);
        CustomAstNode[] array = pathSelect!CustomAstNode(arg).filter!(a => a.name == strName).array;
        assert(array.length == 1);
        return array[0];
    }

    CustomAstNode tryGetCustom(const(char)* name, AAstNode arg) {
        import std.algorithm, std.array;

        string strName = to!string(name);
        CustomAstNode[] array = pathSelect!CustomAstNode(arg).filter!(a => a.name == strName).array;
        if (array.length == 1) {
            return array[0];
        } else {
            assert(array.length == 0);
            return null;
        }
    }

}

extern (C) {

    Node getUniqueChild(Node)(AAstNode arg) {
        Node[] array = pathSelect!Node(arg);
        assert(array.length == 1);
        return array[0];
    }

    Node tryGetUniqueChild(Node)(AAstNode arg) {
        Node[] array = pathSelect!Node(arg);
        if (array.length == 1) {
            return array[0];
        } else {
            assert(array.length == 0);
            return null;
        }
    }

    AAstNode[] extensionGetAll(Node)(AAstNode arg) {
        return cast(AAstNode[]) pathSelect!Node(arg);
    }

}
