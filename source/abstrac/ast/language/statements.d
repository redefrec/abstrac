module abstrac.ast.language.statements;

import abstrac.ast : TAstNode;

class ReturnStatement : TAstNode!ReturnStatement {
}

class ScopeGuardStatement : TAstNode!ScopeGuardStatement {
}

class StaticSingleAssignmentStatement : TAstNode!StaticSingleAssignmentStatement {
}

class IfThenBlock : TAstNode!IfThenBlock {
}

class IfElseBlock : TAstNode!IfElseBlock {
}

class IfStatement : TAstNode!IfStatement {
}

class WhileLoopStatement : TAstNode!WhileLoopStatement {
}

class WhileLoopCondition : TAstNode!WhileLoopCondition {
}

class WhileLoopBody : TAstNode!WhileLoopBody {
}

class ExpressionStatement : TAstNode!ExpressionStatement {
}

class StaticAssertStatement : TAstNode!StaticAssertStatement {
}

class StaticIfStatementThenBlock : TAstNode!StaticIfStatementThenBlock {
}

class StaticIfStatementElseBlock : TAstNode!StaticIfStatementElseBlock {
}

class StaticIfStatement : TAstNode!StaticIfStatement {
}

class Statement : TAstNode!Statement {
}
