module abstrac.ast.language.expressions;

import abstrac.ast : TAstNode, ClonedProperty;

class DecIntegerLiteral : TAstNode!DecIntegerLiteral {
}

class NegatableDecIntegerLiteral : TAstNode!NegatableDecIntegerLiteral {
}

class CharacterLiteral : TAstNode!CharacterLiteral {
}

class BuiltinExpression : TAstNode!BuiltinExpression {
}

class PrefixExpression : TAstNode!PrefixExpression {
}

class PostfixCallOperator : TAstNode!PostfixCallOperator {
}

class PostfixExpression : TAstNode!PostfixExpression {
}

class InfixExpression : TAstNode!InfixExpression {
}

class ProcessedValueNode : TAstNode!ProcessedValueNode {
    import abstrac.processed_values : ProcessedValue;

    @ClonedProperty ProcessedValue processedValue;
}

class Expression : TAstNode!Expression {
}
