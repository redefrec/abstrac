module abstrac.ast.language.declarations;

import abstrac.ast : TAstNode;
import abstrac.processed_types;

class OverloadSet : TAstNode!OverloadSet {
    TemplateDeclaration[] templates;
    FunctionDeclaration[] functions;
}

class TemplateConstraint : TAstNode!TemplateConstraint {
}

class TemplateDeclaration : TAstNode!TemplateDeclaration {
}

class TemplateInstance : TAstNode!TemplateInstance {
}

class TemplateInstantiation : TAstNode!TemplateInstantiation {
}

class StructDeclarationBody : TAstNode!StructDeclarationBody {
}

class StructDeclaration : TAstNode!StructDeclaration {
}

class PrecomputeDeclaration : TAstNode!PrecomputeDeclaration {
}

class TemplateParameterVariadicSuffix : TAstNode!TemplateParameterVariadicSuffix {
}

class TemplateTypeParameterSpecialization : TAstNode!TemplateTypeParameterSpecialization {
}

class TemplateTypeParameter : TAstNode!TemplateTypeParameter {
}

class TemplateValueParameterSpecialization : TAstNode!TemplateValueParameterSpecialization {
}

class TemplateValueParameter : TAstNode!TemplateValueParameter {
}

class TemplateParameterList : TAstNode!TemplateParameterList {
}

class FunctionParameter : TAstNode!FunctionParameter {
}

class VarArgsFunctionParameter : TAstNode!VarArgsFunctionParameter {
}

class FunctionBody : TAstNode!FunctionBody {
}

class FunctionDeclaration : TAstNode!FunctionDeclaration {
}

class AliasDeclaration : TAstNode!AliasDeclaration {
}
