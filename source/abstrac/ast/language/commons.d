module abstrac.ast.language.commons;

import abstrac.ast : TAstNode, AAstNode, ClonedProperty;

class CustomAstNode : TAstNode!CustomAstNode {
    @ClonedProperty string name;
}

class Identifier : TAstNode!Identifier {
}

class BoundIdentifier : TAstNode!BoundIdentifier {
    @ClonedProperty AAstNode declaration;
}

struct Import {
    PreliminaryUnit prelim;
    bool isPublic;
}

class Unit : TAstNode!Unit {
    bool emitted;
    Import[] imports;
}

class PreliminaryUnit : TAstNode!PreliminaryUnit {
    bool emitted;
    Import[] imports;
}

class UnitImport : TAstNode!UnitImport {
}

class PublicVisibility : TAstNode!PublicVisibility {
}
