module abstrac.ast.language.extension;

import abstrac.ast : TAstNode, ClonedProperty;

class Extension : TAstNode!Extension {
}

class ExtensionGrammarProperty : TAstNode!ExtensionGrammarProperty {
}

class ExtensionHooksProperty : TAstNode!ExtensionHooksProperty {
}

class ExtensionHook : TAstNode!ExtensionHook {
}

class ExtensionManipulationProperty : TAstNode!ExtensionManipulationProperty {
}

class ExtensionManipulationVisit : TAstNode!ExtensionManipulationVisit {
}

class CustomInfixOperator : TAstNode!CustomInfixOperator {
    @ClonedProperty long precedence;
    @ClonedProperty bool leftAssociative;
}
