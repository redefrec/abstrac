module abstrac.ast.language.types;

import abstrac.ast : TAstNode, ClonedProperty;

class BuiltinType : TAstNode!BuiltinType {
}

class ProcessedTypeNode : TAstNode!ProcessedTypeNode {
    import abstrac.processed_types : ProcessedType;

    @ClonedProperty ProcessedType processedType;
}

class Type : TAstNode!Type {
}
