module abstrac.templates.deduction;

import abstrac.templates.commons;
import abstrac.templates.instantiation;
import abstrac.utils.multi_dispatch;
import abstrac.utils.stack;
import abstrac.utils.visitor;
import abstrac.type_processor;

struct TemplateArgumentDeducer {
    ModuleBuilder mBuilder;

    ProcessedElement[] deduce(TemplateBinding binding, ProcessedType[] argumentTypes) {
        DeductionState state = DeductionState(mBuilder, binding, argumentTypes);
        auto params = getUniqueChild!TemplateParameterList(binding.template_);
        size_t paramCount = getParamCount(params);
        if (paramCount == binding.arguments.length) {
            return binding.arguments;
        }
        auto nodes = pathSelect!(FunctionDeclaration, FunctionParameter, Type)(binding.template_);
        test(nodes.length == argumentTypes.length, "Invalid number of arguments!");
        foreach (i; 0 .. nodes.length) {
            onlyChild(nodes[i]).accept(new DeductionVisitor(&state, argumentTypes[i]));
        }
        ProcessedElement[] result = binding.arguments.dup;
        foreach (i; result.length .. paramCount) {
            dispatch((TemplateTypeParameter asTypeParam) {
                test((asTypeParam in state.types) !is null,
                    "Couldn't deduce template argument: " ~ asTypeParam.value);
                result ~= state.types[asTypeParam];
            }, (TemplateValueParameter asValueParam) {
                test((asValueParam in state.values) !is null,
                    "Couldn't deduce template argument: " ~ asValueParam.value);
                result ~= state.values[asValueParam];
            })(params.child(i));
        }
        return result;
    }
}

private struct DeductionState {
    ModuleBuilder mBuilder;
    TemplateBinding binding;
    ProcessedType[] argumentTypes;
    ProcessedType[TemplateTypeParameter] types;
    ProcessedValue[TemplateValueParameter] values;
}

private class DeductionVisitor : TVisitor!true {
    DeductionState* state;
    Stack!ProcessedType typeStack;
    Stack!ProcessedValue valueStack;

    this(DeductionState* state, ProcessedType fullType) {
        this.state = state;
        typeStack.push(fullType);
    }

    private void error(string message) {
        throw new TemplatingException(message);
    }

    private void check(bool condition, string message) {
        if (!condition) {
            error(message);
        }
    }

    private void subDeduceType(ProcessedType processed, AAstNode node) {
        size_t lengthBefore = typeStack.length;
        typeStack.push(processed);
        node.accept(this);
        size_t lengthAfter = typeStack.length;
        check(lengthBefore == lengthAfter || lengthAfter == lengthBefore + 1,
                "Stack corruption during template argument deduction!");
        if (lengthAfter > lengthBefore) {
            typeStack.pop();
        }
    }

    private void subDeduceValue(ProcessedValue processed, AAstNode node) {
        size_t lengthBefore = valueStack.length;
        valueStack.push(processed);
        node.accept(this);
        size_t lengthAfter = valueStack.length;
        check(lengthBefore == lengthAfter || lengthAfter == lengthBefore + 1,
                "Stack corruption during template argument deduction!");
        if (lengthAfter > lengthBefore) {
            valueStack.pop();
        }
    }

    private bool matchesIdentifier(BoundIdentifier identifier, TemplateDeclaration template_) {
        return dispatch((OverloadSet asOverloadSet) {
            import std.algorithm : canFind;

            return asOverloadSet.templates.canFind(template_);
        }, (TemplateDeclaration asTemplate) { return asTemplate is template_; })(
                identifier.declaration);
    }

    private Type substituteAlias(AliasDeclaration alias_,
            TemplateParameterList params, TemplateInstantiation instantiation) {
        Type copy = cast(Type) getUniqueChild!Type(alias_).clone();
        void substitute(AAstNode parameter, AAstNode replacement) {
            class LocalVisitor : TVisitor!false {
                override void visit(BoundIdentifier arg) {
                    if (arg.declaration == parameter) {
                        replace(arg, replacement.clone());
                    }
                }
            }

            copy.accept(new LocalVisitor);
        }

        foreach (i; 0 .. getParamCount(params)) {
            dispatch((TemplateTypeParameter asTypeParam) {
                substitute(asTypeParam, instantiation.child(i));
            }, (TemplateValueParameter asValueParam) {
                substitute(asValueParam, instantiation.child(i));
            }, (TemplateConstraint _) {})(params.child(i));
        }
        return copy;
    }

    override void visit(Type arg) {
        visitChildren(arg);
    }

    override void visit(BoundIdentifier arg) {
        tryDispatch((TemplateTypeParameter asTypeParam) {
            auto top = typeStack.pop();
            if (asTypeParam in state.types) {
                check(top == state.types[asTypeParam],
                    "Conflicting types during template argument deduction: " ~ top.toString()
                    ~ " - " ~ state.types[asTypeParam].toString());
            } else {
                state.types[asTypeParam] = top;
            }
        }, (TemplateValueParameter asValueParam) {
            auto top = valueStack.pop();
            if (asValueParam in state.values) {
                check(top == state.values[asValueParam],
                    "Conflicting types during template argument deduction: " ~ top.toString()
                    ~ " - " ~ state.values[asValueParam].toString());
            } else {
                state.values[asValueParam] = top;
            }
        })(arg.declaration);
    }

    override void visit(Expression arg) {
        auto asId = tryGetUniqueChild!BoundIdentifier(arg);
        if (asId !is null) {
            asId.accept(this);
        }
    }

    override void visit(TemplateInstantiation arg) {
        auto top = typeStack.pop();
        auto asId = cast(BoundIdentifier) arg.lastChild();
        check(asId !is null, "Template expression was not a bound identifier!");
        auto template_ = cast(TemplateDeclaration) asId.declaration;
        check(template_ !is null, "Bound identifier did not resolve to template!");
        auto eponym = getEponym(template_);
        bool dispatched = tryDispatch((AliasDeclaration asAlias) {
            Type substituted = substituteAlias(asAlias,
                getUniqueChild!TemplateParameterList(template_), arg);
            subDeduceType(top, substituted);
        }, (StructDeclaration _) {
            auto asStruct = cast(ProcessedStructType) top;
            check(asStruct !is null, "Argument for struct template is not struct typed!");
            auto instance = findAncestorByType!TemplateInstance(asStruct.declaration);
            check(instance !is null, "Unexpected type for template instantiation!");
            auto instantiator = TemplateInstantiator(state.mBuilder);
            auto binding = instantiator.getTemplateBindingOf(instance);
            check(matchesIdentifier(asId, binding.template_),
                "Templates do not match: " ~ asId.declaration.toString() ~ " != " ~ binding.template_.toString());
            size_t i = 0;
            foreach (boundArgument; binding.arguments) {
                if (auto asType = cast(ProcessedType) boundArgument) {
                    subDeduceType(asType, arg.child(i++));
                } else if (auto asValue = cast(ProcessedValue) boundArgument) {
                    subDeduceValue(asValue, arg.child(i++));
                } else {
                    assert(0);
                }
            }
        })(eponym);
        check(dispatched, "Unexpected template eponym: " ~ eponym.toString());
    }

    override void visit(BuiltinType arg) {
        auto top = typeStack.pop();
        switch (arg.value) {
        case "ptr": {
                auto asPtr = cast(ProcessedPointerType) top;
                check(asPtr !is null, "Unexpected type for pointer parameter!");
                auto pointedTypeNode = pathSelect!(TemplateInstantiation, Type)(arg)[0];
                subDeduceType(asPtr.pointedType, pointedTypeNode);
                break;
            }
        case "array": {
                import std.bigint : BigInt;

                auto asArray = cast(ProcessedArrayType) top;
                check(asArray !is null, "Template argument deduction failed!");
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                auto sizeEvaluation = ProcessedIntegerValue.get(ProcessedIntegerType.get(false,
                        64), BigInt(asArray.size));
                auto sizeNode = getUniqueChild!Expression(instantiation);
                subDeduceValue(sizeEvaluation, sizeNode);
                auto typeNode = getUniqueChild!Type(instantiation);
                subDeduceType(asArray.elementType, typeNode);
                break;
            }
        case "setmod": {
                check(top.modifiers.length > 0, "Template argument deduction failed!");
                auto typeNodes = pathSelect!(TemplateInstantiation, Type)(arg);
                auto modifier = TypeProcessor(state.mBuilder).process(typeNodes[1]);
                auto unmodified = top.unsetModifier(modifier);
                subDeduceType(unmodified, typeNodes[0]);
                break;
            }
        case "setmetadata": {
                // TODO: MetaData is ignored!
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                // auto key = TypeProcessor(state.mBuilder).process(cast(Type) instantiation.child(1));
                // auto unset = top.unsetMetaData(key);
                subDeduceType(top, instantiation.child(0));
                break;
            }
        case "function": {
                auto instantiation = getUniqueChild!TemplateInstantiation(arg);
                auto funType = cast(ProcessedFunctionType) top;
                check(funType !is null,
                        "Unexpected type for function parameter during template argument deduction: " ~ top.toString());

                subDeduceType(funType.returnType, instantiation.child(0));
                check(funType.paramTypes.length == instantiation.children.length - 1,
                        "Invalid number of parameters for function parameter during template argument deduction!");
                foreach (i; 1 .. instantiation.children.length) {
                    subDeduceType(funType.paramTypes[i - 1], instantiation.child(i));
                }
                break;
            }
        default:
            error("Cannot deduce template arguments from builtin type: " ~ arg.value);
            assert(0);
        }
    }
}
