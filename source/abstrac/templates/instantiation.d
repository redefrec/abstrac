module abstrac.templates.instantiation;

import abstrac.templates.commons;
public import abstrac.templates.commons : TemplateBinding, TemplatingException;
import abstrac.utils.visitor;
import abstrac.codegen.evaluation;
import abstrac.type_processor;
import abstrac.utils.multi_dispatch;
import std.typecons, std.algorithm, std.range, std.array, std.conv;

private class Cache : IModuleCache {
    TemplateInstance[Tuple!(TemplateDeclaration, ProcessedElement[])] instances;
    ProcessedElement[][TemplateInstantiation] instantiationArguments;
    Nullable!(TemplateInstantiator.MatchResult)[Tuple!(TemplateDeclaration, ProcessedElement[])] matchResults;
    TemplateBinding[TemplateInstance] instanceTemplateMap;
}

struct TemplateInstantiator {
    ModuleBuilder mBuilder;

    private @property Cache cache() {
        // TODO: Fix ignoring of metadata for instance caching
        return new Cache;
        // return mBuilder.getOrCreateCache!Cache();
    }

    TemplateBinding getTemplateBindingOf(TemplateInstance instance) {
        return cache.instanceTemplateMap[instance];
    }

    TemplateInstance instantiate(TemplateBinding binding) {
        if (auto existing = tuple(binding.template_, binding.arguments) in cache.instances) {
            return *existing;
        }
        AAstNode clone = binding.template_.clone();
        TemplateParameterList params = getUniqueChild!TemplateParameterList(clone);
        processIds(clone, binding.template_, params, binding.arguments);
        auto mangleSuffix = "__tph_" ~ to!string(hashOf(binding.arguments));
        TemplateInstance instance = new TemplateInstance;
        cache.instanceTemplateMap[instance] = binding;
        instance.parent = binding.template_.parent;
        assert(binding.template_.children.length == 2);
        AAstNode declaration = clone.child(1);
        declaration.value = binding.template_.value ~ mangleSuffix;
        moveChild(declaration, instance);
        cache.instances[tuple(binding.template_, binding.arguments)] = instance;
        return instance;
    }

    TemplateBinding[] instantiate(AAstNode declaration, ProcessedElement[] arguments) {
        return dispatch((OverloadSet asOverloadSet) {
            return eliminate(asOverloadSet, arguments);
        }, (TemplateDeclaration asTemplate) {
            auto params = getUniqueChild!TemplateParameterList(asTemplate);
            auto paramCount = getParamCount(params);
            if (paramCount > arguments.length) {
                return [TemplateBinding(asTemplate, 0, false, arguments)];
            } else {
                return [TemplateBinding(asTemplate, 0, true, arguments)];
            }
        })(declaration);
    }

    TemplateBinding[] instantiate(TemplateInstantiation node) {
        BoundIdentifier identifier = getUniqueChild!BoundIdentifier(node);
        ProcessedElement[] arguments = evaluateTemplateArguments(node);
        return instantiate(identifier.declaration, arguments);
    }

    TemplateBinding[] deduce(TemplateBinding[] set, ProcessedType[] argTypes) {
        TemplateBinding[] result = [];
        foreach (binding; set) {
            try {
                import abstrac.templates.deduction;

                auto deducer = TemplateArgumentDeducer(mBuilder);
                auto deducedArgs = deducer.deduce(binding, argTypes);
                auto matchResult = match(binding.template_, deducedArgs);
                if (matchResult.matched && matchResult.complete) {
                    result ~= TemplateBinding(binding.template_,
                            matchResult.matchedSpecializations, true, deducedArgs);
                }
            } catch (TemplatingException exc) {
                import std.stdio : writeln;

                writeln(exc.message);
            }
        }
        return result;
    }

    ProcessedType[] getFunctionParamTypes(TemplateBinding binding) {
        auto instance = instantiate(binding);
        auto function_ = cast(FunctionDeclaration) getEponym(instance);
        auto typeNodes = pathSelect!(FunctionParameter, Type)(function_);
        return map!(a => TypeProcessor(mBuilder).process(a))(typeNodes).array;
    }

    ProcessedType getReturnType(TemplateBinding binding) {
        assert(binding.complete);
        Type[] selection = pathSelect!(FunctionDeclaration, Type)(binding.template_);
        TemplateParameterList params = getUniqueChild!TemplateParameterList(binding.template_);
        assert(selection.length == 1);
        auto returnTypeNodeClone = cast(Type) selection[0].clone();
        processIds(returnTypeNodeClone, null, params, binding.arguments);
        return TypeProcessor(mBuilder).process(returnTypeNodeClone);
    }

    TemplateInstance finalize(TemplateBinding[] arg) {
        import std.stdio : writeln;

        if (arg.length != 1) {
            writeln("Expected one valid template; Got: ", arg.length);
            foreach (binding; arg) {
                printAst(binding.template_);
            }
            assert(0);
        }
        return instantiate(arg[0]);
    }

    private static struct MatchResult {
        bool matched;
        bool complete;
        size_t matchedSpecializations;
    }

    private auto match(TemplateDeclaration template_, ProcessedElement[] args) {
        if (auto existing = tuple(template_, args) in cache.matchResults) {
            if ((*existing).isNull) {
                throw new Error("Circular reference during template argument matching!");
            } else {
                return (*existing).get;
            }
        }
        cache.matchResults[tuple(template_, args)] = Nullable!MatchResult.init;
        MatchResult result;
        scope (exit) {
            cache.matchResults[tuple(template_, args)] = result;
        }
        auto paramList = getUniqueChild!TemplateParameterList(template_);
        auto paramListClone = cast(TemplateParameterList) paramList.clone();
        paramListClone.parent = paramList.parent;
        size_t parameterCount = getParamCount(paramListClone);
        if (parameterCount > args.length) {
            result = MatchResult(true, false);
            return result;
        } else if (parameterCount < args.length) {
            result = MatchResult(false);
            return result;
        }
        processIds(paramListClone, null, paramListClone, args);
        int specializationMatches = 0;
        auto constraint = tryGetUniqueChild!TemplateConstraint(paramListClone);
        if (constraint !is null) {
            Expression constraintExpression = getUniqueChild!Expression(constraint);
            auto evaluation = Evaluator(mBuilder).evaluate(constraintExpression);
            auto asInteger = cast(ProcessedIntegerValue) evaluation;
            assert(asInteger !is null);
            if (asInteger.value == 0) {
                result = MatchResult(false);
                return result;
            }
            specializationMatches++;
        }
        for (size_t i = 0; i < parameterCount; ++i) {
            auto param = paramListClone.child(i);
            bool noMatch = false;
            class LocalVisitor : TVisitor!true {
                override void visit(TemplateTypeParameter asTypeParam) {
                    if (auto asTypeArg = cast(ProcessedType) args[i]) {
                        auto specialization = tryGetUniqueChild!TemplateTypeParameterSpecialization(
                                asTypeParam);
                        if (specialization !is null) {
                            Type specializedTypeNode = getUniqueChild!Type(specialization);
                            ProcessedType specializedType = TypeProcessor(mBuilder).process(
                                    specializedTypeNode);
                            if (specializedType == asTypeArg) {
                                specializationMatches += 2;
                            } else {
                                noMatch = true;
                            }
                        }
                    } else {
                        noMatch = true;
                    }
                }

                override void visit(TemplateValueParameter asValueParam) {
                    if (auto asValueArg = cast(ProcessedValue) args[i]) {
                        auto specialization = tryGetUniqueChild!TemplateValueParameterSpecialization(
                                asValueParam);
                        if (specialization !is null) {
                            auto evaluator = Evaluator(mBuilder);
                            auto specializedValue = evaluator.evaluate(specialization.child(0));
                            if (specializedValue == asValueArg) {
                                specializationMatches += 2;
                            } else {
                                noMatch = true;
                            }
                        }
                    } else {
                        noMatch = true;
                    }
                }
            }

            param.accept(new LocalVisitor);
            if (noMatch) {
                result = MatchResult(false);
                return result;
            }
        }
        result = MatchResult(true, true, specializationMatches);
        return result;
    }

    private TemplateBinding[] eliminate(OverloadSet overloadSet, ProcessedElement[] args) {
        TemplateBinding[] matches = [];
        foreach (template_; overloadSet.templates) {
            auto matchResult = match(template_, args);
            if (!matchResult.matched) {
                continue;
            }
            matches ~= TemplateBinding(template_,
                    matchResult.matchedSpecializations, matchResult.complete, args);
        }
        return reduce(matches);
    }

    private TemplateBinding[] reduce(TemplateBinding[] bindings) {
        TemplateBinding[] result = [];
        size_t maxSpecs = 0;
        foreach (binding; bindings) {
            maxSpecs = binding.complete && binding.specs > maxSpecs ? binding.specs : maxSpecs;
        }
        foreach (binding; bindings) {
            if (!binding.complete || binding.specs == maxSpecs) {
                result ~= binding;
            }
        }
        return result;
    }

    private void processIds(AAstNode clone, TemplateDeclaration original,
            TemplateParameterList params, ProcessedElement[] arguments) {
        import abstrac.ast.makers : makeTypeChild, makeExpression;

        class LocalVisitor : TVisitor!false {
            override void visit(BoundIdentifier arg) {
                if (original !is null && arg.declaration == clone) {
                    arg.declaration = original;
                    return;
                }
                size_t parameterCount = getParamCount(params);
                assert(parameterCount == arguments.length);
                for (size_t i = 0; i < parameterCount; i++) {
                    auto param = params.child(i);
                    if (arg.declaration != param) {
                        continue;
                    }
                    auto argument = arguments[i];
                    if (auto asTypeParam = cast(TemplateTypeParameter) param) {
                        auto asType = cast(ProcessedType) argument;
                        assert(asType !is null);
                        replace(arg, makeTypeChild(asType));
                        return;
                    } else if (auto asValueParam = cast(TemplateValueParameter) param) {
                        auto asValue = cast(ProcessedValue) argument;
                        assert(asValue !is null);
                        replace(arg, makeExpression(asValue));
                        return;
                    } else {
                        assert(0);
                    }
                }
            }
        }

        clone.accept(new LocalVisitor);
    }

    private ProcessedElement[] evaluateTemplateArguments(TemplateInstantiation arg) {
        if (auto existing = arg in cache.instantiationArguments) {
            if (*existing is null) {
                throw new Error("Circular reference in template instantiation arguments!");
            } else {
                return *existing;
            }
        }
        cache.instantiationArguments[arg] = null;
        ProcessedElement[] arguments = [];
        foreach (child; arg.children[0 .. $ - 1]) {
            if (!tryDispatch((Type asType) {
                    arguments ~= TypeProcessor(mBuilder).process(asType);
                }, (Expression asExpression) {
                    arguments ~= Evaluator(mBuilder).evaluate(asExpression);
                })(child)) {
                printAst(child.parent);
                throw new Exception("Evaluation error.");
            }
        }
        cache.instantiationArguments[arg] = arguments.dup;
        return arguments;
    }

}
