module abstrac.templates.commons;

public import abstrac.llvm.modules;
public import abstrac.ast;
public import abstrac.ast.helpers;
public import abstrac.processed_values;

struct TemplateBinding {
    TemplateDeclaration template_;
    size_t specs;
    bool complete;
    ProcessedElement[] arguments;
}

class TemplatingException : Exception {
    this(string message) {
        super(message);
    }
}

package void test(bool condition, string message) {
    if (!condition) {
        throw new TemplatingException(message);
    }
}

package size_t getParamCount(TemplateParameterList arg) {
    if (tryGetUniqueChild!TemplateConstraint(arg) is null) {
        return arg.children.length;
    } else {
        return arg.children.length - 1;
    }
}
